#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused)]
#![allow(clippy::all)]

include!(concat!(env!("OUT_DIR"), "/openrm_bindings.rs"));

// Extra missing stuffs from kernel only side
pub const NV_ESC_NUMA_INFO: u32 = NV_IOCTL_BASE + 15;
pub const NV_ESC_SET_NUMA_STATUS: u32 = NV_IOCTL_BASE + 16;

pub const NV_IOCTL_NUMA_INFO_MAX_OFFLINE_ADDRESSES: usize = 64;

#[derive(Clone, Copy, Debug)]
#[repr(align(8))]
pub struct nv_offline_addresses_t {
    pub addresses: [u64; NV_IOCTL_NUMA_INFO_MAX_OFFLINE_ADDRESSES],
    pub num_entries: u32,
}

#[derive(Clone, Copy, Debug)]
#[repr(align(8))]
pub struct nv_ioctl_numa_info_t {
    pub nid: i32,
    pub status: i32,
    pub memblock_size: u64,
    pub numa_mem_addr: u64,
    pub numa_mem_size: u64,
    pub use_auto_online: u8,
    pub offline_addresses: nv_offline_addresses_t,
}
