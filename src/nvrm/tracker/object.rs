use std::collections::HashMap;
use std::sync::Mutex;

use libc::{c_ulong, c_void};

use crate::openrm_bindings::*;

#[derive(Copy, Clone, Debug)]
pub enum HandleType {
    Object,
    Memory,
    DmaContext,
}

#[derive(Copy, Clone, Debug)]
pub enum RawAllocInfoArg {
    Object(Option<*mut c_void>),
    Memory(nv_ioctl_nvos02_parameters_with_fd),
    DmaContext(NVOS39_PARAMETERS),
}

#[derive(Copy, Clone, Debug)]
pub struct RawAllocInfo {
    pub parent_handle: NvHandle,
    pub new_handle: NvHandle,
    pub class_id: u32,
    pub handle_type: HandleType,
}

#[derive(Copy, Clone, Debug)]
pub struct RawFreeInfo {
    pub parent_handle: NvHandle,
    pub old_handle: NvHandle,
}

lazy_static::lazy_static! {
    static ref REGISTRY_OBJECTS: Mutex<HashMap<NvHandle, RawAllocInfo>> = {
        Mutex::new(HashMap::new())
    };
}

pub fn get_object_info(object_handle: NvHandle) -> Option<RawAllocInfo> {
    let registry = REGISTRY_OBJECTS.lock().unwrap();

    registry.get(&object_handle).copied()
}

pub fn get_raw_alloc_object_info_from_arg(
    request: c_ulong,
    arg: *mut c_void,
) -> Option<(RawAllocInfo, RawAllocInfoArg)> {
    let request_function = (request & 0xff) as u32;
    let request_arg_size = ((request >> 16) & 0x1fff) as usize;

    match request_function {
        NV_ESC_RM_ALLOC_MEMORY => {
            let arg = unsafe { &*(arg as *const nv_ioctl_nvos02_parameters_with_fd) };

            Some((
                RawAllocInfo {
                    parent_handle: arg.params.hObjectParent,
                    new_handle: arg.params.hObjectNew,
                    class_id: arg.params.hClass,
                    handle_type: HandleType::Memory,
                },
                RawAllocInfoArg::Memory(*arg),
            ))
        }

        NV_ESC_RM_ALLOC => {
            if request_arg_size == std::mem::size_of::<NVOS21_PARAMETERS>() {
                let arg = unsafe { &*(arg as *const NVOS21_PARAMETERS) };

                Some((
                    RawAllocInfo {
                        parent_handle: arg.hObjectParent,
                        new_handle: arg.hObjectNew,
                        class_id: arg.hClass,
                        handle_type: HandleType::Object,
                    },
                    RawAllocInfoArg::Object(Some(arg.pAllocParms)),
                ))
            } else if request_arg_size == std::mem::size_of::<NVOS64_PARAMETERS>() {
                let arg = unsafe { *(arg as *const NVOS64_PARAMETERS) };

                Some((
                    RawAllocInfo {
                        parent_handle: arg.hObjectParent,
                        new_handle: arg.hObjectNew,
                        class_id: arg.hClass,
                        handle_type: HandleType::Object,
                    },
                    RawAllocInfoArg::Object(Some(arg.pAllocParms)),
                ))
            } else {
                None
            }
        }

        NV_ESC_RM_ALLOC_OBJECT => {
            let arg = unsafe { &*(arg as *const NVOS05_PARAMETERS) };

            Some((
                RawAllocInfo {
                    parent_handle: arg.hObjectParent,
                    new_handle: arg.hObjectNew,
                    class_id: arg.hClass,
                    handle_type: HandleType::Object,
                },
                RawAllocInfoArg::Object(None),
            ))
        }

        NV_ESC_RM_ALLOC_CONTEXT_DMA2 => {
            let arg = unsafe { &*(arg as *const NVOS39_PARAMETERS) };

            Some((
                RawAllocInfo {
                    parent_handle: arg.hObjectParent,
                    new_handle: arg.hObjectNew,
                    class_id: arg.hClass,
                    handle_type: HandleType::DmaContext,
                },
                RawAllocInfoArg::DmaContext(*arg),
            ))
        }
        _ => None,
    }
}

pub fn get_raw_free_object_info_from_arg(
    request: c_ulong,
    arg: *mut c_void,
) -> Option<RawFreeInfo> {
    let request_function = (request & 0xff) as u32;

    if request_function == NV_ESC_RM_FREE {
        let arg = unsafe { &*(arg as *const NVOS00_PARAMETERS) };

        if arg.status == 0 {
            return Some(RawFreeInfo {
                parent_handle: arg.hObjectParent,
                old_handle: arg.hObjectOld,
            });
        }
    }

    None
}

pub(crate) fn handle_ioctl(request: c_ulong, arg: *mut c_void) {
    if let Some((alloc_info, _)) = get_raw_alloc_object_info_from_arg(request, arg) {
        let mut registry = REGISTRY_OBJECTS.lock().unwrap();
        registry.insert(alloc_info.new_handle, alloc_info);
    } else if let Some(free_info) = get_raw_free_object_info_from_arg(request, arg) {
        let mut registry = REGISTRY_OBJECTS.lock().unwrap();
        registry.remove(&free_info.old_handle);
    }
}
