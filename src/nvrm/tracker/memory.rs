use std::collections::HashMap;
use std::sync::Mutex;

use libc::c_int;
use libc::{c_ulong, c_void};

use crate::openrm_bindings::*;
use crate::FileDescriptorInfo;

#[derive(Copy, Clone, Debug)]
pub struct GpuMemoryMapping {
    pub va_handle: NvHandle,
    pub handle: NvHandle,
    pub address: u64,
    pub size: u64,
}

#[derive(Copy, Clone, Debug)]
pub struct CpuRawMapping {
    pub cpu_handle: NvHandle,
    pub address: Option<u64>,
    pub fd: Option<c_int>,
    pub size: u64,
}

#[derive(Copy, Clone, Debug)]
pub struct DmaMapping {
    pub cpu_handle: NvHandle,
    pub gpu_handle: NvHandle,
    pub address: u64,
    pub size: u64,
}

impl DmaMapping {
    pub fn contains(&self, address: u64) -> bool {
        (self.address..self.address + self.size).contains(&address)
    }
}

lazy_static::lazy_static! {
    static ref REGISTRY_GPU_MAPPING: Mutex<HashMap<NvHandle, GpuMemoryMapping>> = {
        Mutex::new(HashMap::new())
    };
}

lazy_static::lazy_static! {
    static ref REGISTRY_CPU_RAW_MAPPING: Mutex<HashMap<NvHandle, CpuRawMapping>> = {
        Mutex::new(HashMap::new())
    };
}

lazy_static::lazy_static! {
    static ref REGISTRY_DMA_MAPPING: Mutex<Vec<DmaMapping>> = {
        Mutex::new(Vec::new())
    };
}

pub fn get_cpu_mapping_by_fd(fd_info: &FileDescriptorInfo) -> Option<CpuRawMapping> {
    let cpu_registry = REGISTRY_CPU_RAW_MAPPING.lock().unwrap();

    for value in cpu_registry.values() {
        if let Some(fd) = value.fd {
            if fd == fd_info.fd {
                return Some(*value);
            }
        }
    }

    None
}

pub fn get_cpu_mapping_by_cpu_handle(cpu_handle: NvHandle) -> Option<(*mut c_void, u64)> {
    let cpu_registry = REGISTRY_CPU_RAW_MAPPING.lock().unwrap();

    if let Some(&CpuRawMapping {
        address: Some(cpu_address),
        size,
        ..
    }) = cpu_registry.get(&cpu_handle)
    {
        return Some((cpu_address as *const c_void as *mut _, size));
    }

    None
}

pub fn get_cpu_mapping_by_gpu_address(gpu_address: u64) -> Option<(*mut c_void, u64)> {
    let registry = REGISTRY_DMA_MAPPING.lock().unwrap();

    for value in registry.iter() {
        if value.contains(gpu_address) {
            let offset = gpu_address - value.address;

            if let Some((cpu_address, _)) = get_cpu_mapping_by_cpu_handle(value.cpu_handle) {
                return Some((
                    (cpu_address as u64 + offset) as *const c_void as *mut _,
                    value.size - offset,
                ));
            }

            return None;
        }
    }

    None
}

fn handle_map_memory(arg: &nv_ioctl_nvos33_parameters_with_fd) {
    let mut registry = REGISTRY_CPU_RAW_MAPPING.lock().unwrap();

    registry.insert(
        arg.params.hMemory,
        CpuRawMapping {
            cpu_handle: arg.params.hMemory,
            address: None,
            size: arg.params.length,
            fd: Some(arg.fd),
        },
    );
}

fn handle_free(arg: &NVOS00_PARAMETERS) {
    {
        let mut registry = REGISTRY_CPU_RAW_MAPPING.lock().unwrap();

        registry.remove(&arg.hObjectOld);
    }

    {
        let mut registry = REGISTRY_GPU_MAPPING.lock().unwrap();

        registry.remove(&arg.hObjectOld);
    }
}

fn handle_vid_heap_control_alloc(
    va_handle: NvHandle,
    arg: &NVOS32_PARAMETERS__bindgen_ty_1__bindgen_ty_1,
) {
    let mut registry = REGISTRY_GPU_MAPPING.lock().unwrap();

    registry.insert(
        arg.hMemory,
        GpuMemoryMapping {
            va_handle,
            handle: arg.hMemory,
            address: arg.offset,
            size: arg.limit,
        },
    );
}

fn handle_vid_heap_control_free(
    va_handle: NvHandle,
    arg: &NVOS32_PARAMETERS__bindgen_ty_1__bindgen_ty_3,
) {
    let mut registry = REGISTRY_GPU_MAPPING.lock().unwrap();

    let res = registry.remove(&arg.hMemory);

    if let Some(res) = res {
        assert!(res.va_handle == va_handle)
    }
}

fn handle_vid_heap_control(arg: &NVOS32_PARAMETERS) {
    match arg.function {
        NVOS32_FUNCTION_ALLOC_SIZE => {
            let params = unsafe { arg.data.AllocSize };

            handle_vid_heap_control_alloc(arg.hVASpace, &params);
        }
        NVOS32_FUNCTION_ALLOC_TILED_PITCH_HEIGHT => {
            let _params = unsafe { arg.data.AllocTiledPitchHeight };

            todo!()
        }
        NVOS32_FUNCTION_FREE => {
            let params = unsafe { arg.data.Free };

            handle_vid_heap_control_free(arg.hVASpace, &params);
        }
        NVOS32_FUNCTION_RELEASE_COMPR => {
            let _params = unsafe { arg.data.ReleaseCompr };

            todo!()
        }
        NVOS32_FUNCTION_REACQUIRE_COMPR => {
            let _params = unsafe { arg.data.ReacquireCompr };

            todo!()
        }
        NVOS32_FUNCTION_INFO => {
            let _params = unsafe { arg.data.Info };

            todo!()
        }
        NVOS32_FUNCTION_DUMP => {
            let _params = unsafe { arg.data.Dump };

            todo!()
        }
        NVOS32_FUNCTION_ALLOC_SIZE_RANGE => {
            let _params = unsafe { arg.data.AllocSizeRange };

            todo!()
        }
        NVOS32_FUNCTION_GET_MEM_ALIGNMENT => {
            let _params = unsafe { arg.data.AllocHintAlignment };

            todo!()
        }
        NVOS32_FUNCTION_HW_ALLOC => {
            let _params = unsafe { arg.data.HwAlloc };

            todo!()
        }
        NVOS32_FUNCTION_HW_FREE => {
            let _params = unsafe { arg.data.HwFree };

            todo!()
        }
        _ => {}
    }
}

fn handle_map_memory_dma(arg: &NVOS46_PARAMETERS) {
    let mut registry = REGISTRY_DMA_MAPPING.lock().unwrap();

    registry.push(DmaMapping {
        cpu_handle: arg.hMemory,
        gpu_handle: arg.hDma,
        address: arg.dmaOffset,
        size: arg.length,
    });
}

fn handle_unmap_memory_dma(arg: &NVOS47_PARAMETERS) {
    let mut registry = REGISTRY_DMA_MAPPING.lock().unwrap();

    let mut target_index_opt = None;

    for (index, value) in registry.iter().enumerate() {
        if value.address == arg.dmaOffset {
            // TODO: handle NV_ESC_RM_DUP_OBJECT as NVIDIA seems to use dup handle interchangeably.
            // assert!(value.cpu_handle == arg.hMemory);
            assert!(value.gpu_handle == arg.hDma);

            target_index_opt = Some(index);
        }
    }

    if let Some(target_index) = target_index_opt {
        registry.remove(target_index);
    }
}

fn handle_update_device_mapping_info(arg: &NVOS56_PARAMETERS) {
    let mut registry = REGISTRY_CPU_RAW_MAPPING.lock().unwrap();

    if let Some(mapping) = registry.get_mut(&arg.hMemory) {
        mapping.address = Some(arg.pNewCpuAddress as u64);
    }
}

pub(crate) fn handle_ioctl(request: c_ulong, arg: *mut c_void) {
    let request_function = (request & 0xff) as u32;

    match request_function {
        NV_ESC_RM_MAP_MEMORY => {
            let arg = unsafe { &*(arg as *const nv_ioctl_nvos33_parameters_with_fd) };

            handle_map_memory(arg);
        }
        NV_ESC_RM_FREE => {
            let arg = unsafe { &*(arg as *const NVOS00_PARAMETERS) };

            handle_free(arg);
        }
        NV_ESC_RM_VID_HEAP_CONTROL => {
            let arg = unsafe { &*(arg as *const NVOS32_PARAMETERS) };

            handle_vid_heap_control(arg);
        }
        NV_ESC_RM_MAP_MEMORY_DMA => {
            let arg = unsafe { &*(arg as *const NVOS46_PARAMETERS) };

            handle_map_memory_dma(arg);
        }
        NV_ESC_RM_UNMAP_MEMORY_DMA => {
            let arg = unsafe { &*(arg as *const NVOS47_PARAMETERS) };

            handle_unmap_memory_dma(arg);
        }
        NV_ESC_RM_UPDATE_DEVICE_MAPPING_INFO => {
            let arg = unsafe { &*(arg as *const NVOS56_PARAMETERS) };

            handle_update_device_mapping_info(arg);
        }
        NV_ESC_RM_CONTROL => {
            let arg = unsafe { &*(arg as *const NVOS54_PARAMETERS) };

            handle_rm_control(arg);
        }
        _ => {}
    }
}

fn handle_rm_control(arg: &NVOS54_PARAMETERS) {
    // Unknown command that is called regularly
    if arg.cmd == 0x2080a097 {
        // pushbuf::dump_all();
    }
}

pub(crate) fn handle_close(fd_info: &FileDescriptorInfo) {
    let mut registry = REGISTRY_CPU_RAW_MAPPING.lock().unwrap();

    for value in registry.values_mut() {
        if let Some(fd) = value.fd {
            if fd == fd_info.fd {
                value.fd = None;
                break;
            }
        }
    }
}

pub(crate) fn handle_mmap(fd_info: &FileDescriptorInfo, addr: *mut c_void, _len: usize) {
    let mut registry = REGISTRY_CPU_RAW_MAPPING.lock().unwrap();

    for value in registry.values_mut() {
        if let Some(fd) = value.fd {
            if fd == fd_info.fd {
                value.address = Some(addr as u64);
                break;
            }
        }
    }
}

pub(crate) fn handle_munmap(_fd: &FileDescriptorInfo, addr: *mut c_void, _len: usize) {
    let mut registry = REGISTRY_CPU_RAW_MAPPING.lock().unwrap();

    for value in registry.values_mut() {
        if let Some(address) = value.address {
            if address == addr as u64 {
                value.address = None;
                break;
            }
        }
    }
}
