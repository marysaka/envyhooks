use std::{
    collections::HashMap,
    fs::File,
    io::Write,
    path::{Path, PathBuf},
    sync::{
        atomic::{AtomicUsize, Ordering},
        Mutex,
    },
};

use crate::{
    hooks::api,
    nvrm::{tracker::memory, utils},
    openrm_bindings::{
        KeplerBControlGPFifo, NvHandle, NV9097_LOAD_MME_INSTRUCTION_RAM_POINTER,
        NV9097_LOAD_MME_START_ADDRESS_RAM_POINTER, NV_CHANNEL_ALLOC_PARAMS,
    },
    FaultHandlerData, FileDescriptorInfo, MemoryWriteTrackerOps,
};
use libc::{c_ulong, c_void};

use super::object::{self, RawAllocInfoArg};

#[derive(Clone, Copy, Debug)]
pub struct GpFifoChannelInfo {
    pub handle: NvHandle,
    pub class_id: u32,
    pub userd_memory_handle: NvHandle,
    pub userd_offset: u64,
    pub gp_fifo_gpu_address: u64,
    pub gp_fifo_entries: u32,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum GpFifoEntryOpcode {
    Nop,
    Illegal,
    GpCrc,
    PbCrc,
    SetPbSegmentExtendedBase,
}

#[derive(Clone, Copy, Debug)]
pub struct GpFifoRawEntry {
    pub opcode: GpFifoEntryOpcode,
    pub sync_wait: bool,
    pub gpu_address: u64,
    pub size: usize,
}

#[derive(Clone, Copy, Debug)]
pub struct PushBufEntry {
    pub gpu_address: u64,
    pub size: usize,
}

impl From<[u32; 2]> for GpFifoRawEntry {
    fn from(value: [u32; 2]) -> Self {
        let entry0 = value[0];
        let entry1 = value[1];

        let opcode = match entry1 & 0x4 {
            1 => GpFifoEntryOpcode::Illegal,
            2 => GpFifoEntryOpcode::GpCrc,
            3 => GpFifoEntryOpcode::PbCrc,
            4 => GpFifoEntryOpcode::SetPbSegmentExtendedBase,
            _ => GpFifoEntryOpcode::Nop,
        };

        let gpu_address = if opcode == GpFifoEntryOpcode::SetPbSegmentExtendedBase {
            ((entry0 >> 8) << 8) as u64
        } else {
            // Assume GP get
            ((entry0 as u64 >> 2) << 2) | ((entry1 as u64 & 0xFF) << 32)
        };

        let size = ((entry1 >> 10) & 0x1fffff) as usize;

        Self {
            opcode,
            sync_wait: (entry1 >> 31) != 0,
            gpu_address,
            size,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Command {
    pub method_id: u32,
    pub subchannel_id: u32,
    pub increment: bool,
    pub data: Vec<u32>,
}

impl Command {
    pub fn parse(raw_data: &[u8]) -> (Option<Command>, &[u8]) {
        if raw_data.len() < 4 {
            return (None, raw_data);
        }

        let command_word = u32::from_ne_bytes(raw_data[0..4].try_into().unwrap());

        let command_type = command_word >> 29;
        let command_count = (command_word >> 16) & 0x1fff;
        let subchannel_id = (command_word >> 13) & 0x7;
        let method_id = (command_word & 0xfff) << 2;

        let increment = command_type == 1 || command_type == 5;

        let (data, data_size) = if command_type == 4 {
            (vec![command_count], 0)
        } else {
            let mut res = Vec::new();

            for i in 0..command_count as usize {
                let start_index = (i + 1) * 4;
                if raw_data[start_index..].len() < 4 {
                    return (None, &[]);
                }

                let value =
                    u32::from_ne_bytes(raw_data[start_index..start_index + 4].try_into().unwrap());

                res.push(value);
            }

            let res_size = res.len();

            (res, res_size)
        };

        let command = Command {
            method_id,
            subchannel_id,
            increment,
            data,
        };

        (Some(command), &raw_data[(data_size + 1) * 4..])
    }
}

#[derive(Clone, Copy, Debug)]
struct GpfifoSubmissionInfo {
    pub channel_info: GpFifoChannelInfo,
    pub gp_get_value: u32,
    pub userd_address: *const KeplerBControlGPFifo,
}

unsafe impl Send for GpfifoSubmissionInfo {}

lazy_static::lazy_static! {
    static ref REGISTRY_FIFO_CHANNELS: Mutex<HashMap<NvHandle, GpFifoChannelInfo>> = {
        Mutex::new(HashMap::new())
    };
}

lazy_static::lazy_static! {
    static ref SUBMISSION_INFO: Mutex<Option<GpfifoSubmissionInfo>> = {
        Mutex::new(None)
    };
}

lazy_static::lazy_static! {
    static ref PUSHBUF_XXHASH64_CACHE: Mutex<Vec<u64>> = {
        Mutex::new(Vec::new())
    };
}

static mut DUMPER_COUNTER: AtomicUsize = AtomicUsize::new(1);

fn get_gpfifo_channel_by_address(address: u64) -> Option<GpFifoChannelInfo> {
    let registry = REGISTRY_FIFO_CHANNELS.lock().unwrap();

    for value in registry.values() {
        if utils::is_class_volta_or_later(value.class_id) {
            if let Some((userd_base_start_address, _)) =
                memory::get_cpu_mapping_by_cpu_handle(value.userd_memory_handle)
            {
                let userd_start_address = userd_base_start_address as u64 + value.userd_offset;
                let control_area_size = std::mem::size_of::<KeplerBControlGPFifo>() as u64;
                if (userd_start_address..userd_start_address + control_area_size).contains(&address)
                {
                    return Some(*value);
                }
            }
        } else if let Some((gpfifo_base_start_address, _)) =
            memory::get_cpu_mapping_by_cpu_handle(value.handle)
        {
            let gpfifo_start_address = gpfifo_base_start_address as u64;
            let control_area_size = std::mem::size_of::<KeplerBControlGPFifo>() as u64;

            if (gpfifo_start_address..gpfifo_start_address + control_area_size).contains(&address) {
                return Some(*value);
            }
        }
    }

    None
}

fn get_gpfifo_channel_by_handle(memory_handle: NvHandle) -> Option<GpFifoChannelInfo> {
    let registry = REGISTRY_FIFO_CHANNELS.lock().unwrap();

    registry.get(&memory_handle).copied()
}

fn handle_gpfifo_channel_write_fault(fault_info: &FaultHandlerData) {
    // Get GPFIFO channel info
    let gpfifo_channel_info = match get_gpfifo_channel_by_address(fault_info.fault_address as u64) {
        Some(value) => value,
        None => {
            let offset = fault_info.fault_address & 0xFF;

            // Only warn if it looks like a GPPut
            if offset == 0x8c {
                eprintln!(
                    "WARNING: GPFIFO channel not found for fault {:?} at address 0x{:x}",
                    fault_info, fault_info.fault_address,
                );
            }

            return;
        }
    };

    // Get GPFIFO base address
    let gpfifo_base_address = if utils::is_class_volta_or_later(gpfifo_channel_info.class_id) {
        match memory::get_cpu_mapping_by_cpu_handle(gpfifo_channel_info.userd_memory_handle) {
            Some((addr, _)) => (addr as u64) + gpfifo_channel_info.userd_offset,
            None => {
                eprintln!(
                    "WARNING: USERD mapping not found for handle {}",
                    gpfifo_channel_info.userd_memory_handle
                );

                return;
            }
        }
    } else {
        match memory::get_cpu_mapping_by_cpu_handle(gpfifo_channel_info.handle) {
            Some((addr, _)) => addr as u64,
            None => {
                eprintln!(
                    "WARNING: GPFIFO mapping not found for handle {}",
                    gpfifo_channel_info.handle
                );

                return;
            }
        }
    };

    let fault_offset = fault_info.fault_address as u64 - gpfifo_base_address;

    // Ensure the address is at GPPut
    if fault_offset != 0x8c {
        eprintln!("WARNING: USERD write not on GPPut: 0x{fault_offset:x}");

        return;
    }

    let userd_address = gpfifo_base_address as *const KeplerBControlGPFifo;

    let mut submission_info = SUBMISSION_INFO.lock().unwrap();

    // Read GPGet to get current pushbuf position and store current info in SUBMISSION_INFO.
    *submission_info = Some(GpfifoSubmissionInfo {
        channel_info: gpfifo_channel_info,
        gp_get_value: unsafe { (*userd_address).GPGet },
        userd_address,
    });
}

fn handle_gpfifo_channel_write_mapping_restore(_fault_info: &FaultHandlerData) {
    let mut submission_info = SUBMISSION_INFO.lock().unwrap();

    if let Some(submission_info) = submission_info.take() {
        let gp_put_value = unsafe { (*submission_info.userd_address).GPPut };

        dump(
            &submission_info.channel_info,
            submission_info.gp_get_value as usize,
            gp_put_value as usize,
        );
    }
}

pub(crate) fn handle_ioctl(request: c_ulong, arg: *mut c_void) {
    if let Some((alloc_info, arg)) = object::get_raw_alloc_object_info_from_arg(request, arg) {
        if let RawAllocInfoArg::Object(Some(param)) = arg {
            if utils::is_gpfifo_class(alloc_info.class_id) {
                let channel_alloc_arg = unsafe { &*(param as *const NV_CHANNEL_ALLOC_PARAMS) };

                let mut registry = REGISTRY_FIFO_CHANNELS.lock().unwrap();

                let value = GpFifoChannelInfo {
                    handle: alloc_info.new_handle,
                    class_id: alloc_info.class_id,
                    userd_memory_handle: channel_alloc_arg.hUserdMemory[0],
                    userd_offset: channel_alloc_arg.userdOffset[0],
                    gp_fifo_gpu_address: channel_alloc_arg.gpFifoOffset,
                    gp_fifo_entries: channel_alloc_arg.gpFifoEntries,
                };

                registry.insert(alloc_info.new_handle, value);

                // On Volta+, the driver map a big memory block that will contain the GPFIFO access.
                if utils::is_class_volta_or_later(alloc_info.class_id) {
                    let (userd_base_mapping_address, _) =
                        match memory::get_cpu_mapping_by_cpu_handle(value.userd_memory_handle) {
                            Some(value) => value,
                            None => {
                                eprintln!(
                                "WARNING: USERD mampping not found for handle {}, memory tracking will not work!",
                                value.userd_memory_handle
                            );

                                return;
                            }
                        };

                    let userd_address = (userd_base_mapping_address as u64) + value.userd_offset;

                    // To be precise on our dumping, we are going to catch GPPut write.
                    // Because GPFIFO channel mapping can be non page aligned, we adjust for that.
                    let page_size = unsafe { libc::sysconf(libc::_SC_PAGE_SIZE) } as u64;
                    let userd_page_aligned = userd_address & !(page_size - 1);

                    let ops = MemoryWriteTrackerOps {
                        on_fault: handle_gpfifo_channel_write_fault,
                        on_restore_mapping: handle_gpfifo_channel_write_mapping_restore,
                    };

                    api::register_write_tracked_region(
                        userd_page_aligned as *mut _,
                        page_size as usize,
                        ops,
                    );
                }
            }
        }
    } else if let Some(free_info) = object::get_raw_free_object_info_from_arg(request, arg) {
        if let Some(object) = object::get_object_info(free_info.old_handle) {
            let mut registry = REGISTRY_FIFO_CHANNELS.lock().unwrap();

            registry.remove(&object.new_handle);
        }
    }
}

pub(crate) fn handle_mmap(fd_info: &FileDescriptorInfo, addr: *mut c_void, _len: usize) {
    if let Some(cpu_mapping) = memory::get_cpu_mapping_by_fd(fd_info) {
        if let Some(gpfifo_channel) = get_gpfifo_channel_by_handle(cpu_mapping.cpu_handle) {
            if utils::is_class_volta_or_later(gpfifo_channel.class_id) {
                eprintln!(
                    "WARNING: USERD mmap for memory handle {} was perform AFTER channel allocation, do we need to change tracking?",
                    gpfifo_channel.handle
                );

                return;
            }

            let page_size = unsafe { libc::sysconf(libc::_SC_PAGE_SIZE) } as u64;

            let ops = MemoryWriteTrackerOps {
                on_fault: handle_gpfifo_channel_write_fault,
                on_restore_mapping: handle_gpfifo_channel_write_mapping_restore,
            };

            api::register_write_tracked_region(addr, page_size as usize, ops);
        };
    }
}

fn dump_pushbuf(
    dump_dir: &Path,
    submission_id: usize,
    entry_index: usize,
    _pushbuf_entry: &PushBufEntry,
    data: &[u8],
) {
    let disable_cache = match std::env::var("EHKS_DISABLE_PUSHBUF_CACHE") {
        Ok(res) => res == "1" || res == "true",
        _ => false,
    };

    if !disable_cache {
        let data_hash = xxhash_rust::xxh3::xxh3_64(data);

        let mut cache = PUSHBUF_XXHASH64_CACHE.lock().unwrap();

        // Check if we the pushbuf data is already in the cache
        if cache.contains(&data_hash) {
            return;
        }

        // We are in a dirty state, let's save the new pushbuf.
        cache.push(data_hash);
    }

    let file_path = dump_dir.join(format!("pushbuf_{submission_id:03}_{entry_index:03}.bin"));

    let mut file = match File::create(&file_path) {
        Ok(file) => file,
        Err(error) => {
            eprintln!("ERROR: Cannot create file for {file_path:?}: {error}");

            return;
        }
    };

    if let Err(error) = file.write_all(data) {
        eprintln!("ERROR: Cannot write to file {file_path:?}: {error}");
    }
}

fn dump_mme_macro(
    dump_dir: &Path,
    submission_id: usize,
    mme_id: u32,
    entry_index: usize,
    command: &Command,
) {
    let file_path = dump_dir.join(format!(
        "mme_{submission_id:03}_{entry_index:03}_{mme_id:03}.bin"
    ));

    let mut file = match File::create(&file_path) {
        Ok(file) => file,
        Err(error) => {
            eprintln!("ERROR: Cannot create file for {file_path:?}: {error}");

            return;
        }
    };

    let data = &command.data[1..];
    let data_slice = unsafe {
        std::slice::from_raw_parts(data.as_ptr() as *const u8, std::mem::size_of_val(data))
    };

    if let Err(error) = file.write_all(data_slice) {
        eprintln!("ERROR: Cannot write to file {file_path:?}: {error}");
    }
}

pub fn dump(gpfifo_channel_info: &GpFifoChannelInfo, start_offset: usize, end_offset: usize) {
    let dump_dir = match std::env::var("EHKS_PUSHBUF_DUMP_DIR") {
        Ok(x) => x,
        _ => return,
    };

    let disable_mme_dump = match std::env::var("EHKS_DISABLE_MME_DUMP") {
        Ok(res) => res == "1" || res == "true",
        _ => false,
    };

    let dump_dir = PathBuf::from(dump_dir);

    if !dump_dir.exists() {
        if let Err(error) = std::fs::create_dir_all(&dump_dir) {
            eprintln!("ERROR: Cannot create directory for {dump_dir:?}: {error}",);

            return;
        }
    }

    let (gp_fifo_address, gp_fifo_size) =
        match memory::get_cpu_mapping_by_gpu_address(gpfifo_channel_info.gp_fifo_gpu_address) {
            Some(value) => value,
            None => {
                eprintln!(
                    "WARNING: GPFIFO entries not found for handle {}",
                    gpfifo_channel_info.handle
                );

                return;
            }
        };

    let gp_fifo_entries = unsafe {
        std::slice::from_raw_parts(gp_fifo_address as *const u32, gp_fifo_size as usize / 4)
    };

    let gp_fifo_entries_offset = start_offset * 2;
    let gp_fifo_entries_count = end_offset * 2;

    let submission_id = unsafe { DUMPER_COUNTER.fetch_add(1, Ordering::AcqRel) };

    for entry_index in (gp_fifo_entries_offset..gp_fifo_entries_count).step_by(2) {
        let raw_entry: [u32; 2] = gp_fifo_entries[entry_index..entry_index + 2]
            .try_into()
            .unwrap();
        let entry = GpFifoRawEntry::from(raw_entry);

        if entry.opcode == GpFifoEntryOpcode::Nop {
            if entry.sync_wait {
                continue;
            }

            // Assume end of GPFIFO.
            if entry.gpu_address == 0 {
                break;
            }
        } else {
            continue;
        }

        let (pushbuf_data_address, _) =
            match memory::get_cpu_mapping_by_gpu_address(entry.gpu_address) {
                Some(value) => value,
                None => {
                    eprintln!("WARNING: GPFIFO entry mapping not found for {entry:?}",);

                    continue;
                }
            };

        let data =
            unsafe { std::slice::from_raw_parts(pushbuf_data_address as *mut u8, entry.size * 4) };

        if data[0..4] == [0, 0, 0, 0] {
            // There is always a command buffer with this size queued around, avoid printing a warning for it
            // TODO: Find a better way to detect it.
            if entry.size != 350 {
                eprintln!("WARNING: GPFIFO data seems invalid, skipping {entry:?}",);
            }

            continue;
        }

        if !disable_mme_dump {
            let mut tmp_data = data;

            let mut mme_id = None;

            loop {
                let (command_opt, res_data) = Command::parse(tmp_data);
                tmp_data = res_data;

                if tmp_data.is_empty() {
                    break;
                }

                if let Some(command) = command_opt {
                    if command.method_id == NV9097_LOAD_MME_START_ADDRESS_RAM_POINTER {
                        mme_id = Some(command.data[0]);
                    }

                    if let Some(mme_id) = mme_id {
                        if command.method_id == NV9097_LOAD_MME_INSTRUCTION_RAM_POINTER {
                            dump_mme_macro(
                                &dump_dir,
                                submission_id,
                                mme_id,
                                entry_index - gp_fifo_entries_offset,
                                &command,
                            );
                        }
                    }
                } else {
                    break;
                }
            }
        }

        let pushbuf_entry = PushBufEntry {
            gpu_address: entry.gpu_address,
            size: entry.size * 4,
        };

        dump_pushbuf(
            &dump_dir,
            submission_id,
            entry_index - gp_fifo_entries_offset,
            &pushbuf_entry,
            data,
        );
    }
}
