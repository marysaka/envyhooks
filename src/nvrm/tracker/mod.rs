use libc::{c_ulong, c_void};

use crate::FileDescriptorInfo;

pub mod memory;
pub mod object;
pub mod pushbuf;

pub(crate) fn handle_ioctl(
    _fd_info: &FileDescriptorInfo,
    request: c_ulong,
    arg: Option<*mut c_void>,
) {
    let arg = match arg {
        Some(arg) => arg,
        None => return,
    };

    memory::handle_ioctl(request, arg);
    pushbuf::handle_ioctl(request, arg);
    object::handle_ioctl(request, arg);
}

pub(crate) fn handle_close(fd_info: &FileDescriptorInfo) {
    memory::handle_close(fd_info);
}

pub(crate) fn handle_mmap(fd_info: FileDescriptorInfo, addr: *mut c_void, len: usize) {
    memory::handle_mmap(&fd_info, addr, len);
    pushbuf::handle_mmap(&fd_info, addr, len);
}

pub(crate) fn handle_munmap(fd_info: FileDescriptorInfo, addr: *mut c_void, len: usize) {
    memory::handle_munmap(&fd_info, addr, len);
}
