use std::ffi::CStr;

use libc::{c_int, c_ulong, c_void, mode_t, off64_t, size_t};

use crate::nvrm::utils::debug_format_ioctl;
use crate::openrm_bindings::*;
use crate::{
    hooks::{api, real},
    FileDescriptorInfo, FileDescriptorOps,
};

mod tracker;
mod utils;

pub fn call_ioctl(fd_info: &FileDescriptorInfo, request: c_ulong, args: *mut c_void) -> c_int {
    let real_ioctl = unsafe { real::get_ioctl() };

    let request_magic = ((request >> 8) & 0xff) as u8;

    let should_log_rm_ioctl = match std::env::var("EHKS_LOG_RM_IOCTL") {
        Ok(res) => res == "1" || res == "true",
        _ => false,
    };

    let should_log = should_log_rm_ioctl && request_magic == NV_IOCTL_MAGIC;

    if should_log {
        eprintln!("DEBUG: BEFORE IOCTL {}", debug_format_ioctl(request, args));
    }

    let res = real_ioctl(fd_info.fd, request, args);

    if should_log {
        eprintln!("DEBUG: AFTER IOCTL {}", debug_format_ioctl(request, args));
    }

    res
}

fn device_ioctl(
    fd_info: FileDescriptorInfo,
    request: c_ulong,
    args: Option<*mut c_void>,
) -> Option<c_int> {
    let request_magic = ((request >> 8) & 0xff) as u8;

    if request_magic != NV_IOCTL_MAGIC {
        return None;
    }

    let res = call_ioctl(&fd_info, request, args.unwrap_or(std::ptr::null_mut()));

    if res >= 0 {
        tracker::handle_ioctl(&fd_info, request, args);
    }

    Some(res)
}

fn device_close(fd_info: FileDescriptorInfo) -> Option<c_int> {
    tracker::handle_close(&fd_info);

    None
}

fn device_mmap(
    fd_info: FileDescriptorInfo,
    addr: *mut c_void,
    len: size_t,
    prot: c_int,
    flags: c_int,
    offset: off64_t,
) -> Option<*mut c_void> {
    let real_mmap = unsafe { real::get_mmap() };

    let res = real_mmap(addr, len, prot, flags, fd_info.fd, offset);

    if !res.is_null() {
        tracker::handle_mmap(fd_info, res, len);
    }

    Some(res)
}

fn device_munmap(fd_info: FileDescriptorInfo, addr: *mut c_void, len: size_t) -> Option<c_int> {
    let real_munmap = unsafe { real::get_munmap() };

    let res = real_munmap(addr, len);

    if res >= 0 {
        tracker::handle_munmap(fd_info, addr, len);
    }

    Some(res)
}

fn open_nvrm(
    _dir_fd_opt: Option<c_int>,
    path: &CStr,
    flags: c_int,
    mode: mode_t,
) -> Option<(c_int, FileDescriptorOps)> {
    let real_open64 = unsafe { real::get_open64() };

    let fd = real_open64(path.as_ptr(), flags, mode);

    if fd <= 0 {
        eprintln!("ERROR: Failed to open {path:?}");
        return None;
    }

    let mut fops = FileDescriptorOps::new();
    fops.close = device_close;
    fops.ioctl = device_ioctl;
    fops.mmap = device_mmap;
    fops.munmap = device_munmap;

    Some((fd, fops))
}

pub fn init() {
    const NVIDIA_PREFIX_NAME: &str = "nvidia";

    api::register_fops_for_path("/dev/nvidiactl", open_nvrm);

    for entry in std::fs::read_dir("/dev").expect("Cannot read_dir on /dev") {
        let entry = entry.unwrap();
        let path = entry.path();
        let file_name = path.file_name().unwrap().to_string_lossy().to_string();
        if file_name.starts_with(NVIDIA_PREFIX_NAME) && file_name != NVIDIA_PREFIX_NAME {
            let file_suffix = &file_name[NVIDIA_PREFIX_NAME.len()..];

            if let Some(c) = file_suffix.chars().next() {
                if c.is_numeric() {
                    api::register_fops_for_path(path.to_str().unwrap(), open_nvrm);
                }
            }
        }
    }
}
