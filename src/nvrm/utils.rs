#![allow(unused)]

use std::{collections::HashMap, fmt::Debug, sync::Mutex};

use libc::{c_ulong, c_void};

use crate::openrm_bindings::*;

type FormatFn = fn(arg: *mut c_void, arg_size: usize) -> String;

macro_rules! register_variant {
    ($value: ident, $request: ident, $arg: ident) => {
        $value.insert(
            $request as _,
            (
                stringify!($request).to_string(),
                format_debug_generic::<$arg> as _,
            ),
        )
    };
}

lazy_static::lazy_static! {
    static ref REGISTRY_IOCTL_FORMAT: Mutex<HashMap<usize, (String, FormatFn)>> = {
        let mut value = HashMap::new();

        value.insert(NV_ESC_CARD_INFO as usize, ("NV_ESC_CARD_INFO".to_string(), (|arg, arg_size| unsafe {
            let device_count = arg_size / std::mem::size_of::<nv_ioctl_card_info_t>();
            format!("{:?}", std::slice::from_raw_parts(arg as *const nv_ioctl_card_info_t, device_count))
        }) as FormatFn));
        register_variant!(value, NV_ESC_CHECK_VERSION_STR, nv_ioctl_rm_api_version_t);
        value.insert(NV_ESC_ATTACH_GPUS_TO_FD as usize, ("NV_ESC_ATTACH_GPUS_TO_FD".to_string(), (|arg, arg_size| unsafe {
            let device_count = arg_size / std::mem::size_of::<u32>();
            format!("{:?}", std::slice::from_raw_parts(arg as *const u32, device_count))
        }) as FormatFn));
        register_variant!(value, NV_ESC_SYS_PARAMS, nv_ioctl_sys_params_t);
        register_variant!(value, NV_ESC_NUMA_INFO, nv_ioctl_numa_info_t);
        register_variant!(value, NV_ESC_RM_ALLOC_MEMORY, nv_ioctl_nvos02_parameters_with_fd);
        register_variant!(value, NV_ESC_RM_ALLOC_OBJECT, NVOS05_PARAMETERS);
        value.insert(NV_ESC_RM_ALLOC as usize, ("NV_ESC_RM_ALLOC".to_string(), format_alloc as FormatFn));
        register_variant!(value, NV_ESC_RM_FREE, NVOS00_PARAMETERS);
        value.insert(NV_ESC_RM_VID_HEAP_CONTROL as usize, ("NV_ESC_RM_VID_HEAP_CONTROL".to_string(), format_vid_heap_control as FormatFn));
        register_variant!(value, NV_ESC_RM_I2C_ACCESS, NVOS_I2C_ACCESS_PARAMS);
        register_variant!(value, NV_ESC_RM_IDLE_CHANNELS, NVOS30_PARAMETERS);
        register_variant!(value, NV_ESC_RM_MAP_MEMORY, nv_ioctl_nvos33_parameters_with_fd);
        register_variant!(value, NV_ESC_RM_UNMAP_MEMORY, NVOS34_PARAMETERS);
        register_variant!(value, NV_ESC_RM_ALLOC_CONTEXT_DMA2, NVOS39_PARAMETERS);
        register_variant!(value, NV_ESC_RM_BIND_CONTEXT_DMA, NVOS49_PARAMETERS);
        register_variant!(value, NV_ESC_RM_MAP_MEMORY_DMA, NVOS46_PARAMETERS);
        register_variant!(value, NV_ESC_RM_UNMAP_MEMORY_DMA, NVOS47_PARAMETERS);
        register_variant!(value, NV_ESC_RM_DUP_OBJECT, NVOS55_PARAMETERS);
        register_variant!(value, NV_ESC_RM_SHARE, NVOS57_PARAMETERS);
        register_variant!(value, NV_ESC_ALLOC_OS_EVENT, nv_ioctl_alloc_os_event_t);
        register_variant!(value, NV_ESC_FREE_OS_EVENT, nv_ioctl_free_os_event_t);
        register_variant!(value, NV_ESC_RM_GET_EVENT_DATA, NVOS41_PARAMETERS);
        register_variant!(value, NV_ESC_STATUS_CODE, nv_ioctl_status_code_t);
        value.insert(NV_ESC_RM_CONTROL as usize, ("NV_ESC_RM_CONTROL".to_string(), format_rm_control as FormatFn));
        register_variant!(value, NV_ESC_RM_UPDATE_DEVICE_MAPPING_INFO, NVOS56_PARAMETERS);
        register_variant!(value, NV_ESC_RM_LOCKLESS_DIAGNOSTIC, NV_LOCKLESS_DIAGNOSTIC_PARAMS);
        register_variant!(value, NV_ESC_REGISTER_FD, nv_ioctl_register_fd_t);

        Mutex::new(value)
    };
}

lazy_static::lazy_static! {
    static ref REGISTRY_RM_CONTROL_FORMAT: Mutex<HashMap<usize, (String, FormatFn)>> = {
        let mut value = HashMap::new();

        register_variant!(value, NV0000_CTRL_CMD_SYSTEM_GET_CPU_INFO, NV0000_CTRL_SYSTEM_GET_CPU_INFO_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_GPU_GET_ID_INFO_V2, NV0000_CTRL_GPU_GET_ID_INFO_V2_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_GPU_GET_ATTACHED_IDS, NV0000_CTRL_GPU_GET_ATTACHED_IDS_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_GPU_GET_DEVICE_IDS, NV0000_CTRL_GPU_GET_DEVICE_IDS_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_GPU_GET_ID_INFO, NV0000_CTRL_GPU_GET_ID_INFO_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_GPU_GET_PROBED_IDS, NV0000_CTRL_GPU_GET_PROBED_IDS_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_GPU_ATTACH_IDS, NV0000_CTRL_GPU_ATTACH_IDS_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_GPU_DETACH_IDS, NV0000_CTRL_GPU_DETACH_IDS_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_GPU_GET_PCI_INFO, NV0000_CTRL_GPU_GET_PCI_INFO_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_GSYNC_GET_ATTACHED_IDS, NV0000_CTRL_GSYNC_GET_ATTACHED_IDS_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_CLIENT_GET_ADDR_SPACE_TYPE, NV0000_CTRL_CLIENT_GET_ADDR_SPACE_TYPE_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_OS_UNIX_EXPORT_OBJECT_TO_FD, NV0000_CTRL_OS_UNIX_EXPORT_OBJECT_TO_FD_PARAMS);
        register_variant!(value, NV0000_CTRL_CMD_OS_UNIX_IMPORT_OBJECT_FROM_FD, NV0000_CTRL_OS_UNIX_IMPORT_OBJECT_FROM_FD_PARAMS);

        register_variant!(value, NV0080_CTRL_CMD_GPU_GET_CLASSLIST_V2, NV0080_CTRL_GPU_GET_CLASSLIST_V2_PARAMS);
        register_variant!(value, NV0080_CTRL_CMD_MSENC_GET_CAPS, NV0080_CTRL_MSENC_GET_CAPS_PARAMS);
        register_variant!(value, NV0080_CTRL_CMD_BSP_GET_CAPS_V2, NV0080_CTRL_BSP_GET_CAPS_PARAMS_V2);
        register_variant!(value, NV0080_CTRL_CMD_FIFO_GET_ENGINE_CONTEXT_PROPERTIES, NV0080_CTRL_FIFO_GET_ENGINE_CONTEXT_PROPERTIES_PARAMS);
        register_variant!(value, NV0080_CTRL_CMD_GPU_GET_VIRTUALIZATION_MODE, NV0080_CTRL_GPU_GET_VIRTUALIZATION_MODE_PARAMS);
        register_variant!(value, NV0080_CTRL_CMD_DMA_ADV_SCHED_GET_VA_CAPS, NV0080_CTRL_DMA_ADV_SCHED_GET_VA_CAPS_PAGE_TABLE_FORMAT);
        register_variant!(value, NV0080_CTRL_CMD_DMA_GET_CAPS, NV0080_CTRL_DMA_GET_CAPS_PARAMS);
        register_variant!(value, NV0080_CTRL_CMD_GPU_GET_NUM_SUBDEVICES, NV0080_CTRL_GPU_GET_NUM_SUBDEVICES_PARAMS);
        register_variant!(value, NV0080_CTRL_CMD_GR_GET_CAPS, NV0080_CTRL_GR_GET_CAPS_PARAMS);
        register_variant!(value, NV0080_CTRL_CMD_GR_GET_INFO, NV0080_CTRL_GR_GET_INFO_PARAMS);
        register_variant!(value, NV0080_CTRL_CMD_FB_GET_CAPS, NV0080_CTRL_FB_GET_CAPS_PARAMS);
        register_variant!(value, NV0080_CTRL_CMD_FIFO_GET_CAPS, NV0080_CTRL_FIFO_GET_CAPS_PARAMS);

        register_variant!(value, NV2080_CTRL_CMD_CE_GET_CAPS_V2, NV2080_CTRL_CE_GET_CAPS_V2_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_GPU_GET_ID, NV2080_CTRL_GPU_GET_ID_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_GPU_GET_ENGINE_PARTNERLIST, NV2080_CTRL_GPU_GET_ENGINE_PARTNERLIST_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_GPU_GET_ENGINES_V2, NV2080_CTRL_GPU_GET_ENGINES_V2_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_GPU_GET_INFO_V2, NV2080_CTRL_GPU_GET_INFO_V2_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_GPU_GET_ENGINES, NV2080_CTRL_GPU_GET_ENGINES_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_GPU_GET_NAME_STRING, NV2080_CTRL_GPU_GET_NAME_STRING_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_GPU_GET_SIMULATION_INFO, NV2080_CTRL_GPU_GET_SIMULATION_INFO_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_GPU_QUERY_ECC_STATUS, NV2080_CTRL_GPU_QUERY_ECC_EXCEPTION_STATUS);
        register_variant!(value, NV2080_CTRL_CMD_BUS_GET_INFO_V2, NV2080_CTRL_BUS_GET_INFO_V2_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_MC_GET_ARCH_INFO, NV2080_CTRL_MC_GET_ARCH_INFO_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_GR_CTXSW_ZCULL_BIND, NV2080_CTRL_GR_CTXSW_ZCULL_BIND_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_PERF_BOOST, NV2080_CTRL_PERF_BOOST_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_FB_GET_INFO, NV2080_CTRL_FB_GET_INFO_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_CE_GET_CE_PCE_MASK, NV2080_CTRL_CE_GET_CE_PCE_MASK_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_BUS_GET_PCI_INFO, NV2080_CTRL_BUS_GET_PCI_INFO_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_FB_GET_GPU_CACHE_INFO, NV2080_CTRL_FB_GET_GPU_CACHE_INFO_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_FB_GET_FB_REGION_INFO, NV2080_CTRL_CMD_FB_GET_FB_REGION_FB_REGION_INFO);
        register_variant!(value, NV2080_CTRL_CMD_GR_GET_ZCULL_INFO, NV2080_CTRL_GR_GET_ZCULL_INFO_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_GR_GET_INFO, NV2080_CTRL_GR_GET_INFO_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_TIMER_GET_TIME, NV2080_CTRL_TIMER_GET_TIME_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_TIMER_GET_GPU_CPU_TIME_CORRELATION_INFO, NV2080_CTRL_TIMER_GET_GPU_CPU_TIME_CORRELATION_INFO_PARAMS);
        register_variant!(value, NV2080_CTRL_CMD_EVENT_SET_NOTIFICATION, NV2080_CTRL_EVENT_SET_NOTIFICATION_PARAMS);

        register_variant!(value, NV906F_CTRL_GET_CLASS_ENGINEID, NV906F_CTRL_GET_CLASS_ENGINEID_PARAMS);
        register_variant!(value, NV9096_CTRL_CMD_GET_ZBC_CLEAR_TABLE_ENTRY, NV9096_CTRL_GET_ZBC_CLEAR_TABLE_ENTRY_PARAMS);
        register_variant!(value, NV9096_CTRL_CMD_SET_ZBC_COLOR_CLEAR, NV9096_CTRL_SET_ZBC_COLOR_CLEAR_PARAMS);
        register_variant!(value, NV9096_CTRL_CMD_GET_ZBC_CLEAR_TABLE_SIZE, NV9096_CTRL_GET_ZBC_CLEAR_TABLE_SIZE_PARAMS);

        register_variant!(value, NVA06F_CTRL_CMD_GPFIFO_SCHEDULE, NVA06F_CTRL_GPFIFO_SCHEDULE_PARAMS);
        register_variant!(value, NVA06F_CTRL_CMD_BIND, NVA06F_CTRL_BIND_PARAMS);

        register_variant!(value, NVC36F_CTRL_CMD_GPFIFO_GET_WORK_SUBMIT_TOKEN, NVC36F_CTRL_CMD_GPFIFO_GET_WORK_SUBMIT_TOKEN_PARAMS);
        register_variant!(value, NVC36F_CTRL_CMD_GPFIFO_SET_WORK_SUBMIT_TOKEN_NOTIF_INDEX, NVC36F_CTRL_GPFIFO_SET_WORK_SUBMIT_TOKEN_NOTIF_INDEX_PARAMS);

        Mutex::new(value)
    };
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
struct DummyEmptyStruct;

lazy_static::lazy_static! {
    static ref REGISTRY_RM_ALLOC_CLASS: Mutex<HashMap<u32, (String, FormatFn)>> = {
        let mut value = HashMap::new();

        register_variant!(value, NV01_ROOT, NV0000_ALLOC_PARAMETERS);
        register_variant!(value, NV04_MEMORY, NV0000_ALLOC_PARAMETERS);
        register_variant!(value, NV01_DEVICE_0, NV0080_ALLOC_PARAMETERS);
        register_variant!(value, NV01_EVENT, NV0005_ALLOC_PARAMETERS);
        register_variant!(value, NV01_EVENT_OS_EVENT, NV0005_ALLOC_PARAMETERS);
        register_variant!(value, NV01_MEMORY_VIRTUAL, NV_MEMORY_VIRTUAL_ALLOCATION_PARAMS);
        register_variant!(value, NV20_SUBDEVICE_0, NV2080_ALLOC_PARAMETERS);
        register_variant!(value, NV2081_BINAPI, NV2081_ALLOC_PARAMETERS);


        register_variant!(value, FERMI_CONTEXT_SHARE_A, NV_CTXSHARE_ALLOCATION_PARAMETERS);
        register_variant!(value, FERMI_VASPACE_A, NV_VASPACE_ALLOCATION_PARAMETERS);

        register_variant!(value, VOLTA_USERMODE_A, DummyEmptyStruct);
        register_variant!(value, TURING_USERMODE_A, DummyEmptyStruct);
        register_variant!(value, AMPERE_USERMODE_A, DummyEmptyStruct);
        register_variant!(value, HOPPER_USERMODE_A, NV_HOPPER_USERMODE_A_PARAMS);

        register_variant!(value, GF100_ZBC_CLEAR, DummyEmptyStruct);
        register_variant!(value, GF100_DISP_SW, NV9072_ALLOCATION_PARAMETERS);

        register_variant!(value, KEPLER_CHANNEL_GROUP_A, NV_CHANNEL_GROUP_ALLOCATION_PARAMETERS);


        // All GPFIFO channels
        register_variant!(value, NV50_CHANNEL_GPFIFO, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);
        register_variant!(value, GF100_CHANNEL_GPFIFO, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);
        register_variant!(value, KEPLER_CHANNEL_GPFIFO_A, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);
        register_variant!(value, KEPLER_CHANNEL_GPFIFO_B, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);
        register_variant!(value, KEPLER_CHANNEL_GPFIFO_C, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);
        register_variant!(value, MAXWELL_CHANNEL_GPFIFO_A, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);
        register_variant!(value, PASCAL_CHANNEL_GPFIFO_A, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);
        register_variant!(value, VOLTA_CHANNEL_GPFIFO_A, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);
        register_variant!(value, TURING_CHANNEL_GPFIFO_A, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);
        register_variant!(value, AMPERE_CHANNEL_GPFIFO_A, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);
        register_variant!(value, HOPPER_CHANNEL_GPFIFO_A, NV_CHANNELGPFIFO_ALLOCATION_PARAMETERS);

        // All DMA channels
        register_variant!(value, MAXWELL_DMA_COPY_A, NVB0B5_ALLOCATION_PARAMETERS);
        register_variant!(value, PASCAL_DMA_COPY_A, NVB0B5_ALLOCATION_PARAMETERS);
        register_variant!(value, VOLTA_DMA_COPY_A, NVB0B5_ALLOCATION_PARAMETERS);
        register_variant!(value, TURING_DMA_COPY_A, NVB0B5_ALLOCATION_PARAMETERS);
        register_variant!(value, AMPERE_DMA_COPY_A, NVB0B5_ALLOCATION_PARAMETERS);
        register_variant!(value, AMPERE_DMA_COPY_B, NVB0B5_ALLOCATION_PARAMETERS);
        register_variant!(value, HOPPER_DMA_COPY_A, NVB0B5_ALLOCATION_PARAMETERS);

        // All I2M channels
        register_variant!(value, KEPLER_INLINE_TO_MEMORY_B, DummyEmptyStruct);

        // All 2D channels
        register_variant!(value, FERMI_TWOD_A, DummyEmptyStruct);

        // All 3D channels
        register_variant!(value, FERMI_A, DummyEmptyStruct);
        register_variant!(value, KEPLER_A, DummyEmptyStruct);
        register_variant!(value, KEPLER_B, DummyEmptyStruct);
        register_variant!(value, MAXWELL_A, DummyEmptyStruct);
        register_variant!(value, MAXWELL_B, DummyEmptyStruct);
        register_variant!(value, PASCAL_A, DummyEmptyStruct);
        register_variant!(value, PASCAL_B, DummyEmptyStruct);
        register_variant!(value, VOLTA_A, DummyEmptyStruct);
        register_variant!(value, TURING_A, DummyEmptyStruct);
        register_variant!(value, AMPERE_A, DummyEmptyStruct);
        register_variant!(value, AMPERE_B, DummyEmptyStruct);
        register_variant!(value, ADA_A, DummyEmptyStruct);
        register_variant!(value, HOPPER_A, DummyEmptyStruct);

        // All COMPUTE channels
        register_variant!(value, KEPLER_COMPUTE_A, DummyEmptyStruct);
        register_variant!(value, KEPLER_COMPUTE_B, DummyEmptyStruct);
        register_variant!(value, MAXWELL_COMPUTE_A, DummyEmptyStruct);
        register_variant!(value, MAXWELL_COMPUTE_B, DummyEmptyStruct);
        register_variant!(value, PASCAL_COMPUTE_A, DummyEmptyStruct);
        register_variant!(value, PASCAL_COMPUTE_B, DummyEmptyStruct);
        register_variant!(value, VOLTA_COMPUTE_A, DummyEmptyStruct);
        register_variant!(value, TURING_COMPUTE_A, DummyEmptyStruct);
        register_variant!(value, AMPERE_COMPUTE_A, DummyEmptyStruct);
        register_variant!(value, AMPERE_COMPUTE_B, DummyEmptyStruct);
        register_variant!(value, ADA_COMPUTE_A, DummyEmptyStruct);
        register_variant!(value, HOPPER_COMPUTE_A, DummyEmptyStruct);

        Mutex::new(value)
    };
}

pub fn debug_format_ioctl(request: c_ulong, arg: *mut c_void) -> String {
    let request_function = (request & 0xff) as usize;
    let request_arg_size = ((request >> 16) & 0x1fff) as usize;

    let registry = REGISTRY_IOCTL_FORMAT.lock().unwrap();

    if let Some((function_name, format_arg)) = registry.get(&request_function) {
        format!("{function_name}({})", format_arg(arg, request_arg_size))
    } else {
        format!("unhandled request_function ({request_function:x})")
    }
}

fn format_debug_generic<T: Copy + Debug>(arg: *mut c_void, _arg_size: usize) -> String {
    if arg.is_null() {
        "(null)".into()
    } else {
        format!("{:?}", unsafe { *(arg as *const T) })
    }
}

fn format_rm_control(arg: *mut c_void, _arg_size: usize) -> String {
    let rm_ioctl_arg: &NVOS54_PARAMETERS = unsafe { &*(arg as *const NVOS54_PARAMETERS) };
    let command_id = rm_ioctl_arg.cmd as usize;

    let registry = REGISTRY_RM_CONTROL_FORMAT.lock().unwrap();

    if let Some((function_name, format_arg)) = registry.get(&command_id) {
        format!(
            "{function_name}({})",
            format_arg(rm_ioctl_arg.params, rm_ioctl_arg.paramsSize as usize)
        )
    } else {
        eprintln!("UNKNOWN CMDID: {command_id:x}");
        format!("{:?}", rm_ioctl_arg)
    }
}

fn format_alloc(arg: *mut c_void, arg_size: usize) -> String {
    let (default_result, class_id, params) = unsafe {
        if arg_size == std::mem::size_of::<NVOS21_PARAMETERS>() {
            let arg = &*(arg as *const NVOS21_PARAMETERS);
            (format!("{:?}", arg), arg.hClass, arg.pAllocParms)
        } else if arg_size == std::mem::size_of::<NVOS64_PARAMETERS>() {
            let arg = *(arg as *const NVOS64_PARAMETERS);
            (format!("{:?}", arg), arg.hClass, arg.pAllocParms)
        } else {
            (
                format!("unknown_arg with size {arg_size:x}"),
                0,
                std::ptr::null_mut(),
            )
        }
    };

    let registry = REGISTRY_RM_ALLOC_CLASS.lock().unwrap();

    if let Some((class_id, format_arg)) = registry.get(&class_id) {
        format!(
            "{default_result} (class_id: {}, params: {})",
            class_id,
            format_arg(params, 0)
        )
    } else {
        format!("{default_result} (unknown class_id: see cl{class_id:04x}.h)")
    }
}

fn format_vid_heap_control(arg: *mut c_void, _arg_size: usize) -> String {
    let arg = unsafe { &*(arg as *const NVOS32_PARAMETERS) };

    let default_result = format!("{arg:?}");

    let params_format = match arg.function {
        NVOS32_FUNCTION_ALLOC_SIZE => {
            let params = unsafe { arg.data.AllocSize };

            format!("{params:?}")
        }
        NVOS32_FUNCTION_ALLOC_TILED_PITCH_HEIGHT => {
            let params = unsafe { arg.data.AllocTiledPitchHeight };

            format!("{params:?}")
        }
        NVOS32_FUNCTION_FREE => {
            let params = unsafe { arg.data.Free };

            format!("{params:?}")
        }
        NVOS32_FUNCTION_RELEASE_COMPR => {
            let params = unsafe { arg.data.ReleaseCompr };

            format!("{params:?}")
        }
        NVOS32_FUNCTION_REACQUIRE_COMPR => {
            let params = unsafe { arg.data.ReacquireCompr };

            format!("{params:?}")
        }
        NVOS32_FUNCTION_INFO => {
            let params = unsafe { arg.data.Info };

            format!("{params:?}")
        }
        NVOS32_FUNCTION_DUMP => {
            let params = unsafe { arg.data.Dump };

            format!("{params:?}")
        }
        NVOS32_FUNCTION_ALLOC_SIZE_RANGE => {
            let params = unsafe { arg.data.AllocSizeRange };

            format!("{params:?}")
        }
        NVOS32_FUNCTION_GET_MEM_ALIGNMENT => {
            let params = unsafe { arg.data.AllocHintAlignment };

            format!("{params:?}")
        }
        NVOS32_FUNCTION_HW_ALLOC => {
            let params = unsafe { arg.data.HwAlloc };

            format!("{params:?}")
        }
        NVOS32_FUNCTION_HW_FREE => {
            let params = unsafe { arg.data.HwFree };

            format!("{params:?}")
        }

        _ => {
            format!("unknown function id: {:x}", arg.function)
        }
    };

    format!("{default_result} ({params_format})")
}

pub fn is_class_volta_or_later(class_id: u32) -> bool {
    let gen_id = (class_id >> 8) & 0xFF;

    gen_id >= 0xC3
}

pub fn is_gpfifo_class(class_id: u32) -> bool {
    class_id == NV50_CHANNEL_GPFIFO
        || class_id == GF100_CHANNEL_GPFIFO
        || class_id == KEPLER_CHANNEL_GPFIFO_A
        || class_id == KEPLER_CHANNEL_GPFIFO_B
        || class_id == KEPLER_CHANNEL_GPFIFO_C
        || class_id == MAXWELL_CHANNEL_GPFIFO_A
        || class_id == PASCAL_CHANNEL_GPFIFO_A
        || class_id == VOLTA_CHANNEL_GPFIFO_A
        || class_id == TURING_CHANNEL_GPFIFO_A
        || class_id == AMPERE_CHANNEL_GPFIFO_A
        || class_id == HOPPER_CHANNEL_GPFIFO_A
}

pub fn is_usermode_class(class_id: u32) -> bool {
    class_id == VOLTA_USERMODE_A
        || class_id == TURING_USERMODE_A
        || class_id == AMPERE_USERMODE_A
        || class_id == HOPPER_USERMODE_A
}

pub fn is_2d_class(class_id: u32) -> bool {
    class_id == FERMI_TWOD_A
}

pub fn is_i2m_class(class_id: u32) -> bool {
    class_id == KEPLER_INLINE_TO_MEMORY_B
}

pub fn is_3d_class(class_id: u32) -> bool {
    class_id == FERMI_A
        || class_id == KEPLER_A
        || class_id == KEPLER_B
        || class_id == MAXWELL_A
        || class_id == MAXWELL_B
        || class_id == PASCAL_A
        || class_id == PASCAL_B
        || class_id == VOLTA_A
        || class_id == TURING_A
        || class_id == AMPERE_A
        || class_id == AMPERE_B
        || class_id == ADA_A
        || class_id == HOPPER_A
}

pub fn is_compute_class(class_id: u32) -> bool {
    class_id == KEPLER_COMPUTE_A
        || class_id == KEPLER_COMPUTE_B
        || class_id == MAXWELL_COMPUTE_A
        || class_id == MAXWELL_COMPUTE_B
        || class_id == PASCAL_COMPUTE_A
        || class_id == PASCAL_COMPUTE_B
        || class_id == VOLTA_COMPUTE_A
        || class_id == TURING_COMPUTE_A
        || class_id == AMPERE_COMPUTE_A
        || class_id == AMPERE_COMPUTE_B
        || class_id == ADA_COMPUTE_A
        || class_id == HOPPER_COMPUTE_A
}

pub fn is_copy_engine_class(class_id: u32) -> bool {
    class_id == KEPLER_DMA_COPY_A
        || class_id == MAXWELL_DMA_COPY_A
        || class_id == PASCAL_DMA_COPY_A
        || class_id == VOLTA_DMA_COPY_A
        || class_id == TURING_DMA_COPY_A
        || class_id == AMPERE_DMA_COPY_A
        || class_id == AMPERE_DMA_COPY_B
        || class_id == HOPPER_DMA_COPY_A
}

pub fn is_engine_class(class_id: u32) -> bool {
    is_gpfifo_class(class_id)
        || is_2d_class(class_id)
        || is_3d_class(class_id)
        || is_compute_class(class_id)
        || is_i2m_class(class_id)
        || is_copy_engine_class(class_id)
}
