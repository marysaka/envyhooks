#![allow(unused)]

use std::{ffi::CString, ops::Range};

use libc::{c_int, c_void, size_t};

use crate::hooks::real;

pub unsafe fn create_fake_fd(flags: c_int) -> c_int {
    let file_path = CString::new("/dev/null").unwrap();
    let real_open = real::get_open();

    real_open(file_path.as_ptr(), flags)
}

pub unsafe fn create_dummy_memory_map(
    addr: *mut c_void,
    len: size_t,
    mut prot: c_int,
) -> Option<Range<usize>> {
    let real_mmap = real::get_mmap();

    let need_commit_memory = prot == libc::PROT_NONE;

    if need_commit_memory {
        prot = libc::PROT_READ | libc::PROT_WRITE;
    }

    let mapping_ptr = real_mmap(addr, len, prot, libc::MAP_PRIVATE | libc::MAP_ANON, -1, 0);

    if mapping_ptr == libc::MAP_FAILED {
        return None;
    }

    if need_commit_memory {
        libc::mprotect(mapping_ptr, len, libc::PROT_NONE);
    }

    Some(mapping_ptr as usize..mapping_ptr as usize + len)
}
