pub mod signal;

use std::ffi::{c_ulong, CStr};

use libc::{c_int, c_void, mode_t, off64_t, size_t, ssize_t};

use crate::{hooks::api, utils};

pub use signal::sigaction;

pub fn init() {
    signal::init();
}

pub fn open(dir_fd_opt: Option<c_int>, path: &CStr, flags: c_int, mode: mode_t) -> Option<c_int> {
    let path_str = path.to_string_lossy().to_string();
    let open = api::get_open_by_path(path_str.clone())?;
    let (mut new_fd, new_fops) = (open)(dir_fd_opt, path, flags, mode)?;

    if new_fd <= 0 {
        new_fd = unsafe { utils::create_fake_fd(flags) };
    }

    assert!(new_fd > 0);

    api::register_fops_for_fd(new_fd, Some(path_str), flags, new_fops);

    Some(new_fd)
}

pub fn read(fd: c_int, data: &mut [u8]) -> Option<ssize_t> {
    let (finfo, fops) = api::get_fops_by_fd(fd)?;

    (fops.read)(finfo, None, data)
}

pub fn write(fd: c_int, data: &[u8]) -> Option<ssize_t> {
    let (finfo, fops) = api::get_fops_by_fd(fd)?;

    (fops.write)(finfo, None, data)
}

pub fn mmap(
    addr: *mut c_void,
    len: size_t,
    prot: c_int,
    flags: c_int,
    fd: c_int,
    offset: off64_t,
) -> Option<*mut c_void> {
    let (finfo, fops) = api::get_fops_by_fd(fd)?;

    (fops.mmap)(finfo, addr, len, prot, flags, offset)
}

pub fn munmap(addr: *mut c_void, len: size_t) -> Option<c_int> {
    let (finfo, fops) = api::get_fops_by_address(addr as usize)?;

    api::unregister_address_range(finfo.clone().mmap_opt.unwrap().memory_range);

    (fops.munmap)(finfo, addr, len)
}

// TODO: map dup
#[allow(dead_code)]
pub fn dup(fd: c_int, newfd: Option<c_int>, _flags: Option<c_int>) -> Option<c_int> {
    let (finfo, fops) = api::get_fops_by_fd(fd)?;

    let newfd = newfd.unwrap_or_else(|| unsafe { utils::create_fake_fd(finfo.initial_flags) });

    api::register_fops_for_fd(newfd, finfo.path_opt, finfo.initial_flags, fops);

    Some(newfd)
}

pub fn ioctl(fd: c_int, request: c_ulong, args: Option<*mut c_void>) -> Option<c_int> {
    let (finfo, fops) = api::get_fops_by_fd(fd)?;

    (fops.ioctl)(finfo, request, args)
}

pub(crate) fn fcntl(fd: c_int, cmd: c_int, arg: Option<c_int>) -> Option<c_int> {
    let (finfo, fops) = api::get_fops_by_fd(fd)?;

    (fops.fcntl)(finfo, cmd, arg)
}

pub fn close(fd: c_int) -> Option<c_int> {
    let (finfo, fops) = api::get_fops_by_fd(fd)?;

    api::unregister_fd(fd);

    (fops.close)(finfo)
}
