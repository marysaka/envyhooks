use std::{collections::HashMap, sync::Mutex, thread::ThreadId};

use libc::{c_int, sigemptyset, siginfo_t, ucontext_t};

use crate::{
    hooks::{api, real},
    FaultHandlerData, NextFaultState,
};

static mut OLD_SIGSEGV_HANDLER: Option<libc::sigaction> = None;
static mut OLD_SIGTRAP_HANDLER: Option<libc::sigaction> = None;

lazy_static::lazy_static! {
    static ref FAULT_INFO_REGISTRY: Mutex<HashMap<ThreadId, FaultHandlerData>> = {
        Mutex::new(HashMap::new())
    };
}

fn set_single_step(context: &mut ucontext_t, enabled: bool) {
    const EFL_TF: i64 = 0x0100;

    let reg_efl = &mut context.uc_mcontext.gregs[libc::REG_EFL as usize];

    if enabled {
        *reg_efl |= EFL_TF;
    } else {
        *reg_efl &= !EFL_TF;
    }
}

unsafe fn does_track_signal(signum: c_int) -> bool {
    matches!(signum, libc::SIGSEGV | libc::SIGTRAP)
}

unsafe fn get_old_signal_handler(signum: c_int) -> Option<&'static libc::sigaction> {
    match signum {
        libc::SIGSEGV => OLD_SIGSEGV_HANDLER.as_ref(),
        libc::SIGTRAP => OLD_SIGTRAP_HANDLER.as_ref(),
        _ => todo!(),
    }
}

unsafe fn set_old_signal_handler(signum: c_int, handler: Option<libc::sigaction>) {
    match signum {
        libc::SIGSEGV => {
            OLD_SIGSEGV_HANDLER = handler;
        }
        libc::SIGTRAP => {
            OLD_SIGTRAP_HANDLER = handler;
        }
        _ => {}
    }
}

// Handle SIGSEGV for write on tracked pages.
unsafe extern "C" fn signal_handler(
    signum: c_int,
    siginfo: *mut siginfo_t,
    context: *mut ucontext_t,
) {
    let mut handled = false;
    let mut handling_done = false;

    let fault_address_opt = if signum == libc::SIGSEGV {
        Some((*siginfo).si_addr() as usize)
    } else {
        None
    };

    let thread_id = std::thread::current().id();

    {
        let mut fault_info_registry = FAULT_INFO_REGISTRY.lock().unwrap();

        match fault_info_registry.get_mut(&thread_id) {
            Some(fault_handler_data) => {
                let next_state = match fault_handler_data.fault_state {
                    NextFaultState::HandleFault => NextFaultState::RestoreMapping,
                    NextFaultState::RestoreMapping => NextFaultState::RestoreMapping,
                };

                // Sanity check the current state vs next state
                if next_state == NextFaultState::RestoreMapping && signum == libc::SIGSEGV {
                    panic!("SIGSEGV when restoring mappings")
                }

                fault_handler_data.fault_state = next_state;
            }
            None => {
                // Initial fault setup
                if let Some(fault_address) = fault_address_opt {
                    if let Some((minfo, mops)) =
                        api::get_write_tracker_ops_by_address(fault_address)
                    {
                        fault_info_registry.insert(
                            thread_id,
                            FaultHandlerData {
                                fault_state: NextFaultState::HandleFault,
                                minfo,
                                mops,
                                fault_address,
                            },
                        );
                    }
                }
            }
        }
    }

    let fault_info_opt = {
        let fault_info_registry = FAULT_INFO_REGISTRY.lock().unwrap();

        fault_info_registry.get(&thread_id).cloned()
    };

    match &fault_info_opt {
        Some(fault_handler_data) => {
            handled = true;

            let fault_state = fault_handler_data.fault_state;
            let mmap_info = &fault_handler_data.minfo;

            match fault_state {
                NextFaultState::HandleFault => {
                    (fault_handler_data.mops.on_fault)(fault_handler_data);

                    // Reprotect as READ_WRITE to make the operation succeed and set single step.
                    libc::mprotect(
                        mmap_info.address(),
                        mmap_info.size(),
                        libc::PROT_READ | libc::PROT_WRITE,
                    );
                    set_single_step(&mut *context, true);
                }
                NextFaultState::RestoreMapping => {
                    (fault_handler_data.mops.on_restore_mapping)(fault_handler_data);

                    // Reprotect as READ only and disable single step.
                    libc::mprotect(mmap_info.address(), mmap_info.size(), libc::PROT_READ);
                    set_single_step(&mut *context, false);

                    handling_done = true;
                }
            }
        }
        None => {}
    }

    if handling_done {
        let mut fault_info_registry = FAULT_INFO_REGISTRY.lock().unwrap();

        fault_info_registry.remove(&thread_id);
    }

    if !handled {
        if let Some(previous) = get_old_signal_handler(signum) {
            if previous.sa_flags & libc::SA_SIGINFO != 0 {
                std::mem::transmute::<
                    usize,
                    extern "C" fn(libc::c_int, *mut libc::siginfo_t, *mut libc::c_void),
                >(previous.sa_sigaction)(signum, siginfo, context as *mut _)
            } else if previous.sa_sigaction == libc::SIG_DFL
                || previous.sa_sigaction == libc::SIG_IGN
            {
                libc::sigaction(signum, previous, std::ptr::null_mut());
            } else {
                std::mem::transmute::<usize, extern "C" fn(libc::c_int)>(previous.sa_sigaction)(
                    signum,
                )
            }
        }
    }
}

pub fn init() {
    let mut old_sigaction: libc::sigaction = unsafe { std::mem::zeroed() };
    let mut new_sigaction: libc::sigaction = unsafe { std::mem::zeroed() };
    new_sigaction.sa_flags = libc::SA_RESTART | libc::SA_SIGINFO | libc::SA_ONSTACK;
    new_sigaction.sa_sigaction = signal_handler as usize;

    unsafe {
        sigemptyset(&mut new_sigaction.sa_mask);

        let real_sigaction = real::get_sigaction();

        real_sigaction(libc::SIGSEGV, &new_sigaction, &mut old_sigaction);
        set_old_signal_handler(libc::SIGSEGV, Some(old_sigaction));

        real_sigaction(libc::SIGTRAP, &new_sigaction, &mut old_sigaction);
        set_old_signal_handler(libc::SIGTRAP, Some(old_sigaction));
    }
}

pub unsafe fn sigaction(
    signum: c_int,
    act: *const libc::sigaction,
    oldact: *mut libc::sigaction,
) -> Option<c_int> {
    if does_track_signal(signum) {
        if !oldact.is_null() {
            *oldact = unsafe { std::mem::zeroed() };

            if let Some(old_handler) = get_old_signal_handler(signum) {
                *oldact = *old_handler;
            }
        }

        if act.is_null() {
            set_old_signal_handler(signum, None);
        } else {
            set_old_signal_handler(signum, Some(*act));
        }

        return Some(0);
    }

    None
}
