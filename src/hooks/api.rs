use crate::{FileDescriptorInfo, FileDescriptorOps, MemoryMapInfo, MemoryWriteTrackerOps, OpenFn};
use libc::{c_int, c_void};
use std::{collections::HashMap, ops::Range, sync::Mutex};

lazy_static::lazy_static! {
    static ref REGISTRY_PATH: Mutex<HashMap<String, OpenFn>> = {
        Mutex::new(HashMap::new())
    };
}

lazy_static::lazy_static! {
    static ref REGISTRY_FDS: Mutex<HashMap<c_int, (FileDescriptorInfo, FileDescriptorOps)>> = {
        Mutex::new(HashMap::new())
    };
}

lazy_static::lazy_static! {
    static ref REGISTRY_MEMORY_RANGES: Mutex<HashMap<Range<usize>, (FileDescriptorInfo, FileDescriptorOps)>> = {
        Mutex::new(HashMap::new())
    };
}

pub fn get_open_by_path(path: String) -> Option<OpenFn> {
    let registry = REGISTRY_PATH.lock().unwrap();

    registry.get(&path).copied()
}

pub fn get_fops_by_fd(fd: c_int) -> Option<(FileDescriptorInfo, FileDescriptorOps)> {
    let registry = REGISTRY_FDS.lock().unwrap();

    registry.get(&fd).cloned()
}

pub fn get_fops_by_address(address: usize) -> Option<(FileDescriptorInfo, FileDescriptorOps)> {
    let registry = REGISTRY_MEMORY_RANGES.lock().unwrap();

    for (range, value) in registry.iter() {
        if range.contains(&address) {
            return Some(value.clone());
        }
    }

    None
}
lazy_static::lazy_static! {
    static ref REGISTRY_MEMORY_WRITE_TRACKER: Mutex<HashMap<Range<usize>, (MemoryMapInfo, MemoryWriteTrackerOps)>> = {
        Mutex::new(HashMap::new())
    };
}

pub fn get_write_tracker_ops_by_address(
    address: usize,
) -> Option<(MemoryMapInfo, MemoryWriteTrackerOps)> {
    let registry = REGISTRY_MEMORY_WRITE_TRACKER.lock().unwrap();

    for (range, value) in registry.iter() {
        if range.contains(&address) {
            return Some(value.clone());
        }
    }

    None
}

pub fn register_write_tracked_region(addr: *mut c_void, size: usize, ops: MemoryWriteTrackerOps) {
    let mut registry = REGISTRY_MEMORY_WRITE_TRACKER.lock().unwrap();

    unsafe {
        // We first do a dummy read.
        // In case the GPU is suspended,
        // we were seeing SIGBUS on the first access to the page in the signal handler.
        // Reason is unknown but it might be something going wrong with the transition.
        std::ptr::read_volatile(addr as *mut u32);

        // Make the region READ only
        let res = libc::mprotect(addr, size, libc::PROT_READ);

        assert!(res == 0);
    }

    let memory_range = addr as usize..addr as usize + size;
    let map_info = MemoryMapInfo {
        memory_range: memory_range.clone(),
    };

    registry.insert(memory_range, (map_info, ops));
}

pub fn unregister_address_range(memory_range: Range<usize>) {
    let mut registry = REGISTRY_MEMORY_RANGES.lock().unwrap();

    registry.remove(&memory_range);
}

pub fn register_fops_for_path(path: &str, open: OpenFn) {
    let mut registry = REGISTRY_PATH.lock().unwrap();

    registry.insert(path.into(), open);
}

pub fn register_fops_for_fd(
    fd: c_int,
    path_opt: Option<String>,
    initial_flags: c_int,
    fops: FileDescriptorOps,
) {
    let finfo = FileDescriptorInfo {
        fd,
        path_opt,
        mmap_opt: None,
        initial_flags,
    };
    let mut registry = REGISTRY_FDS.lock().unwrap();

    registry.insert(fd, (finfo, fops));
}

pub fn unregister_fd(fd: c_int) {
    let mut registry = REGISTRY_FDS.lock().unwrap();

    registry.remove(&fd);
}
