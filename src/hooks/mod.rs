pub mod api;
mod detail;

use std::ffi::CStr;

use libc::{c_char, c_int, c_ulong, c_void, mode_t, off64_t, off_t, size_t, ssize_t};

pub fn init() {
    detail::init();
}

pub mod real {
    use libc::{
        c_char, c_int, c_ulong, c_void, dlsym, off64_t, off_t, sigaction, size_t, ssize_t,
        RTLD_NEXT,
    };
    use std::{ffi::CString, sync::Once};

    pub unsafe fn get_open() -> extern "C" fn(*const c_char, c_int, ...) -> c_int {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("open").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<*const c_void, extern "C" fn(*const c_char, c_int, ...) -> c_int>(
            REAL_FN_PTR,
        )
    }

    pub unsafe fn get_open64() -> extern "C" fn(*const c_char, c_int, ...) -> c_int {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("open64").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<*const c_void, extern "C" fn(*const c_char, c_int, ...) -> c_int>(
            REAL_FN_PTR,
        )
    }

    pub unsafe fn get_openat() -> extern "C" fn(c_int, *const c_char, c_int, ...) -> c_int {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("openat").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<*const c_void, extern "C" fn(c_int, *const c_char, c_int, ...) -> c_int>(
            REAL_FN_PTR,
        )
    }

    pub unsafe fn get_openat64() -> extern "C" fn(c_int, *const c_char, c_int, ...) -> c_int {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("openat64").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<*const c_void, extern "C" fn(c_int, *const c_char, c_int, ...) -> c_int>(
            REAL_FN_PTR,
        )
    }

    pub unsafe fn get_close() -> extern "C" fn(c_int) -> c_int {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("close").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<*const c_void, extern "C" fn(c_int) -> c_int>(REAL_FN_PTR)
    }

    pub unsafe fn get_ioctl() -> extern "C" fn(c_int, c_ulong, ...) -> c_int {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("ioctl").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<*const c_void, extern "C" fn(c_int, c_ulong, ...) -> c_int>(
            REAL_FN_PTR,
        )
    }

    pub unsafe fn get_fcntl() -> extern "C" fn(c_int, c_int, ...) -> c_int {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("fcntl").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<*const c_void, extern "C" fn(c_int, c_int, ...) -> c_int>(REAL_FN_PTR)
    }

    pub unsafe fn get_read() -> extern "C" fn(c_int, *mut c_void, size_t) -> ssize_t {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("read").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<*const c_void, extern "C" fn(c_int, *mut c_void, size_t) -> ssize_t>(
            REAL_FN_PTR,
        )
    }

    pub unsafe fn get_write() -> extern "C" fn(c_int, *const c_void, size_t) -> ssize_t {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("write").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<*const c_void, extern "C" fn(c_int, *const c_void, size_t) -> ssize_t>(
            REAL_FN_PTR,
        )
    }

    pub unsafe fn get_mmap(
    ) -> extern "C" fn(*mut c_void, size_t, c_int, c_int, c_int, off_t) -> *mut c_void {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("mmap").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<
            *const c_void,
            extern "C" fn(*mut c_void, size_t, c_int, c_int, c_int, off_t) -> *mut c_void,
        >(REAL_FN_PTR)
    }

    pub unsafe fn get_mmap64(
    ) -> extern "C" fn(*mut c_void, size_t, c_int, c_int, c_int, off64_t) -> *mut c_void {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("mmap64").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<
            *const c_void,
            extern "C" fn(*mut c_void, size_t, c_int, c_int, c_int, off64_t) -> *mut c_void,
        >(REAL_FN_PTR)
    }

    pub unsafe fn get_munmap() -> extern "C" fn(*mut c_void, size_t) -> c_int {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("munmap").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<*const c_void, extern "C" fn(*mut c_void, size_t) -> c_int>(
            REAL_FN_PTR,
        )
    }

    pub unsafe fn get_sigaction() -> extern "C" fn(c_int, *const sigaction, *mut sigaction) -> c_int
    {
        static mut REAL_FN_PTR: *const c_void = core::ptr::null();
        static REAL_FN_ONCE: Once = Once::new();

        REAL_FN_ONCE.call_once(|| {
            let symbol_cstr = CString::new("sigaction").unwrap();
            REAL_FN_PTR = dlsym(RTLD_NEXT, symbol_cstr.as_ptr());
        });

        std::mem::transmute::<
            *const c_void,
            extern "C" fn(c_int, *const sigaction, *mut sigaction) -> c_int,
        >(REAL_FN_PTR)
    }
}

#[no_mangle]
pub unsafe extern "C" fn open(path: *const c_char, oflag: c_int, mut variadic: ...) -> c_int {
    let mode = variadic.arg::<mode_t>();

    match detail::open(None, CStr::from_ptr(path), oflag, mode) {
        Some(res) => res,
        None => {
            let real_open = real::get_open();

            real_open(path, oflag, mode)
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn open64(path: *const c_char, oflag: c_int, mut variadic: ...) -> c_int {
    let mode = variadic.arg::<mode_t>();

    match detail::open(None, CStr::from_ptr(path), oflag, mode) {
        Some(res) => res,
        None => {
            let real_open64 = real::get_open64();

            real_open64(path, oflag, mode)
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn openat(
    dirfd: c_int,
    path: *const c_char,
    oflag: c_int,
    mut variadic: ...
) -> c_int {
    let mode = variadic.arg::<mode_t>();

    match detail::open(Some(dirfd), CStr::from_ptr(path), oflag, mode) {
        Some(res) => res,
        None => {
            let real_openat = real::get_openat();

            real_openat(dirfd, path, oflag, mode)
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn openat64(
    dirfd: c_int,
    path: *const c_char,
    oflag: c_int,
    mut variadic: ...
) -> c_int {
    let mode = variadic.arg::<mode_t>();

    match detail::open(Some(dirfd), CStr::from_ptr(path), oflag, mode) {
        Some(res) => res,
        None => {
            let real_openat64 = real::get_openat64();

            real_openat64(dirfd, path, oflag, mode)
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn close(fd: c_int) -> c_int {
    match detail::close(fd) {
        Some(res) => res,
        None => {
            let real_close = real::get_close();

            real_close(fd)
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn read(fd: c_int, buf: *mut c_void, count: size_t) -> ssize_t {
    match detail::read(fd, core::slice::from_raw_parts_mut(buf as *mut _, count)) {
        Some(res) => res,
        None => {
            let real_read = real::get_read();

            real_read(fd, buf, count)
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn write(fd: c_int, buf: *const c_void, count: size_t) -> ssize_t {
    match detail::write(fd, core::slice::from_raw_parts(buf as *const _, count)) {
        Some(res) => res,
        None => {
            let real_write = real::get_write();

            real_write(fd, buf, count)
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn ioctl(fd: c_int, request: c_ulong, mut variadic: ...) -> c_int {
    let request_arg_size = (request >> 16) & 0x1fff;

    let args = match request_arg_size {
        0 => None,
        _ => Some(variadic.arg::<*mut c_void>()),
    };

    match detail::ioctl(fd, request, args) {
        Some(res) => res,
        None => {
            let real_ioctl = real::get_ioctl();

            match args {
                Some(value) => real_ioctl(fd, request, value),
                None => real_ioctl(fd, request),
            }
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn fcntl(fd: c_int, cmd: c_int, mut variadic: ...) -> c_int {
    let has_int_arg = cmd == libc::F_DUPFD
        || cmd == libc::F_SETFD
        || cmd == libc::F_DUPFD_CLOEXEC
        || cmd == libc::F_SETFL;

    let arg = if has_int_arg {
        Some(variadic.arg::<c_int>())
    } else {
        None
    };

    // TODO: forward F_DUPFD and F_DUPFD_CLOEXEC to dup

    match detail::fcntl(fd, cmd, arg) {
        Some(res) => res,
        None => {
            let real_fcntl = real::get_fcntl();

            match arg {
                Some(arg) => real_fcntl(fd, cmd, arg),
                None => real_fcntl(fd, cmd),
            }
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn mmap(
    addr: *mut c_void,
    len: size_t,
    prot: c_int,
    flags: c_int,
    fd: c_int,
    offset: off_t,
) -> *mut c_void {
    match detail::mmap(addr, len, prot, flags, fd, offset) {
        Some(res) => res,
        None => {
            let real_mmap = real::get_mmap();

            real_mmap(addr, len, prot, flags, fd, offset)
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn mmap64(
    addr: *mut c_void,
    len: size_t,
    prot: c_int,
    flags: c_int,
    fd: c_int,
    offset: off64_t,
) -> *mut c_void {
    match detail::mmap(addr, len, prot, flags, fd, offset) {
        Some(res) => res,
        None => {
            let real_mmap64 = real::get_mmap64();

            real_mmap64(addr, len, prot, flags, fd, offset)
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn munmap(addr: *mut c_void, len: size_t) -> c_int {
    match detail::munmap(addr, len) {
        Some(res) => res,
        None => {
            let real_munmap = real::get_munmap();

            real_munmap(addr, len)
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn sigaction(
    signum: c_int,
    act: *const libc::sigaction,
    oldact: *mut libc::sigaction,
) -> c_int {
    match detail::sigaction(signum, act, oldact) {
        Some(res) => res,
        None => {
            let real_sigaction = real::get_sigaction();

            real_sigaction(signum, act, oldact)
        }
    }
}
