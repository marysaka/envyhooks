#![feature(c_variadic)]

use std::{ffi::CStr, ops::Range};

use libc::{c_int, c_ulong, c_void, mode_t, off64_t, size_t, ssize_t};

mod hooks;
mod nvrm;
mod openrm_bindings;
mod utils;

#[derive(Clone, Debug)]
pub struct MemoryMapInfo {
    pub memory_range: Range<usize>,
}

impl MemoryMapInfo {
    pub fn address(&self) -> *mut c_void {
        self.memory_range.start as *mut c_void
    }

    pub fn size(&self) -> usize {
        self.memory_range.len()
    }
}

#[derive(Clone, Debug)]
pub struct FileDescriptorInfo {
    pub fd: c_int,
    pub path_opt: Option<String>,
    pub mmap_opt: Option<MemoryMapInfo>,
    pub initial_flags: c_int,
}

pub type OpenFn = fn(
    dir_fd_opt: Option<c_int>,
    path: &CStr,
    flags: c_int,
    mode: mode_t,
) -> Option<(c_int, FileDescriptorOps)>;

#[derive(Clone, Debug)]
pub struct FileDescriptorOps {
    pub close: fn(fd_info: FileDescriptorInfo) -> Option<c_int>,
    pub read: fn(
        fd_info: FileDescriptorInfo,
        offset_opt: Option<usize>,
        data: &mut [u8],
    ) -> Option<ssize_t>,
    pub write:
        fn(fd_info: FileDescriptorInfo, offset_opt: Option<usize>, data: &[u8]) -> Option<ssize_t>,
    pub ioctl: fn(
        fd_info: FileDescriptorInfo,
        request: c_ulong,
        args: Option<*mut c_void>,
    ) -> Option<c_int>,
    pub fcntl: fn(fd_info: FileDescriptorInfo, cmd: c_int, value: Option<c_int>) -> Option<c_int>,
    pub mmap: fn(
        fd_info: FileDescriptorInfo,
        addr: *mut c_void,
        len: size_t,
        prot: c_int,
        flags: c_int,
        offset: off64_t,
    ) -> Option<*mut c_void>,
    pub munmap: fn(fd_info: FileDescriptorInfo, addr: *mut c_void, len: size_t) -> Option<c_int>,
}

impl Default for FileDescriptorOps {
    fn default() -> Self {
        Self::new()
    }
}

impl FileDescriptorOps {
    pub fn new() -> Self {
        FileDescriptorOps {
            close: |_| None,
            read: |_, _, _| None,
            write: |_, _, _| None,
            ioctl: |_, _, _| None,
            fcntl: |_, _, _| None,
            mmap: |_, _, _, _, _, _| None,
            munmap: |_, _, _| None,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum NextFaultState {
    HandleFault,
    RestoreMapping,
}

#[derive(Clone, Debug)]
pub struct FaultHandlerData {
    pub fault_state: NextFaultState,
    pub minfo: MemoryMapInfo,
    pub mops: MemoryWriteTrackerOps,
    pub fault_address: usize,
}

unsafe impl Send for FaultHandlerData {}

#[derive(Clone, Debug)]
pub struct MemoryWriteTrackerOps {
    pub on_fault: fn(fault_handler_data: &FaultHandlerData),
    pub on_restore_mapping: fn(fault_handler_data: &FaultHandlerData),
}

extern "C" fn initialize_library() {
    hooks::init();
    nvrm::init();
}

#[link_section = ".init_array"]
#[used]
pub static LIBRARY_INITIALIZER: extern "C" fn() = initialize_library;
