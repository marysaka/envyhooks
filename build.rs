use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rerun-if-changed=wrapper.h");

    let common_include_path = PathBuf::new()
        .join("externals")
        .join("open-gpu-kernel-modules")
        .join("src")
        .join("common")
        .join("sdk")
        .join("nvidia")
        .join("inc");

    let common_arch_include_path = PathBuf::new()
        .join("externals")
        .join("open-gpu-kernel-modules")
        .join("src")
        .join("nvidia")
        .join("arch")
        .join("nvalloc")
        .join("unix")
        .join("include");

    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .impl_debug(true)
        .clang_arg(format!("-I{}", common_include_path.display()))
        .clang_arg(format!("-I{}", common_arch_include_path.display()))
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("openrm_bindings.rs"))
        .expect("Couldn't write bindings!");
}
