# envyhooks

This is a hooking library to dump command buffers from the NVIDIA drivers.


## Installing

This require rustup (or a nightly version of rustc for c_variadic), libclang and NVIDIA drivers version **560.35.03** (it might work with prior or later version but this is the version used for current testing)
```sh
git clone --recursive https://gitlab.freedesktop.org/marysaka/envyhooks.git
cd envyhooks
cargo build
```


## Running

Example to run with vkcube

```sh
LD_PRELOAD=./target/debug/libenvyhooks.so EHKS_PUSHBUF_DUMP_DIR=$PWD/dump_output vkcube
```

## Environment variables

### EHKS_PUSHBUF_DUMP_DIR

Path to the directory to dump command buffers and MME blobs.

### EHKS_DISABLE_MME_DUMP

By default, envyhooks will try to dump MME from command buffers.

This disable this behavior.

### EHKS_DISABLE_PUSHBUF_CACHE

By default, envyhooks will try to deduplicate command buffers dumped.

This disable this behavior.

### EHKS_LOG_RM_IOCTL

Enable logging ioctls communication between NVIDIA kernel driver and the userland.

NOTE: This doesn't support nvidia-uvm or nvidia-modeset.
