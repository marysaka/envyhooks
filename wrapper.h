// RM ioctls
#include <nv-ioctl.h>

// Old private RM ioctls
#include <nv_escape.h>
#include <nv-ioctl-lockless-diag.h>
#include <nv-unix-nvos-params-wrappers.h>

// USERMODE_A
#include <class/clc361.h>
#include <class/clc461.h>
#include <class/clc561.h>
#include <class/clc661.h>


#define INCLUDE_CLASS
#define INCLUDE_CTRL

// Auto generated list of ctrl and class import
#ifdef INCLUDE_CLASS
#include "class/cl0071.h"
#include "class/cl00f8.h"
#include "class/cla06fsw.h"
#include "class/cl9010.h"
#include "class/cl90f1.h"
#include "class/clc0b5.h"
#include "class/cl0092_callback.h"
#include "class/cl0001.h"
#include "class/clc3b0.h"
#include "class/clb0c0.h"
#include "class/cl9271.h"
#include "class/cl9270.h"
#include "class/cl00fe.h"
#include "class/clc56fsw.h"
#include "class/cl927c.h"
#include "class/cla081.h"
#include "class/cla084_notification.h"
#include "class/clc770.h"
#include "class/cl9097.h"
#include "class/clc6b0.h"
#include "class/clc67a.h"
#include "class/clc7fa.h"
#include "class/cl947d.h"
#include "class/cl000f.h"
#include "class/clb1c0.h"
#include "class/cl0073.h"
#include "class/cl90ec.h"
#include "class/clc6b5.h"
#include "class/clc57a.h"
#include "class/cla0c0.h"
#include "class/clc370_notification.h"
#include "class/cl003e.h"
#include "class/clc57e.h"
#include "class/cl30f1_notification.h"
#include "class/clb8b0.h"
#include "class/clc372sw.h"
#include "class/cla0c0qmd.h"
#include "class/clcb33.h"
#include "class/cl00b1.h"
#include "class/clc56f.h"
#include "class/cl9072.h"
#include "class/clc06f.h"
#include "class/clc37b.h"
#include "class/cl00c1.h"
#include "class/clb06f.h"
#include "class/cl917cswspare.h"
#include "class/cl2080.h"
#include "class/clcba2.h"
#include "class/cl0000_notification.h"
#include "class/clb0cc.h"
#include "class/clc46fsw.h"
#include "class/clc67d.h"
#include "class/clc3b5.h"
#include "class/cl00f2.h"
#include "class/clc574.h"
#include "class/cl0080.h"
#include "class/clc197.h"
#include "class/clc370.h"
#include "class/clc365.h"
#include "class/clc597.h"
#include "class/cl9072_notification.h"
#include "class/clb06fsw.h"
#include "class/clc3c0.h"
#include "class/cl0030.h"
#include "class/cl90cdfecs.h"
#include "class/clc997.h"
#include "class/cl0060.h"
#include "class/cl0090.h"
#include "class/clc697.h"
#include "class/cl0041.h"
#include "class/clc7b0.h"
#include "class/cl506f.h"
#include "class/cl9171.h"
#include "class/cl9471.h"
#include "class/clc67e.h"
#include "class/cla097.h"
#include "class/clc9c0.h"
#include "class/cl5080.h"
#include "class/cla16f.h"
#include "class/cl00db.h"
#include "class/cl2080_notification.h"
#include "class/clc57d.h"
#include "class/clc36fsw.h"
#include "class/cl84a0.h"
#include "class/cl0042.h"
#include "class/clcbc0.h"
#include "class/cl917a.h"
#include "class/clc773.h"
#include "class/cl90cc.h"
#include "class/clc1c0.h"
#include "class/clc0b5sw.h"
#include "class/cl2082.h"
#include "class/clc1b0.h"
#include "class/clc8b5.h"
#include "class/cl85b5sw.h"
#include "class/clc06fsw.h"
#include "class/clb097tex.h"
#include "class/clb0b0.h"
#include "class/cl977d.h"
#include "class/cl844c.h"
#include "class/clc1b5sw.h"
#include "class/clc361.h"
#include "class/cl00da.h"
#include "class/cla26f.h"
#include "class/cl906f.h"
#include "class/cl9870.h"
#include "class/cla06f.h"
#include "class/clc4d1.h"
#include "class/clc371.h"
#include "class/clb8fa.h"
#include "class/clc77f.h"
#include "class/clc1b5.h"
#include "class/cl9770.h"
#include "class/cl9010_callback.h"
#include "class/cl50a0.h"
#include "class/clc369.h"
#include "class/clc6c0.h"
#include "class/cl917c.h"
#include "class/cla084.h"
#include "class/clc2b0.h"
#include "class/cl84a0_deprecated.h"
#include "class/cla06fsubch.h"
#include "class/cl00f3.h"
#include "class/cl0040.h"
#include "class/clb2cc.h"
#include "class/cla197.h"
#include "class/cl902d.h"
#include "class/clc0b7.h"
#include "class/clc3b7.h"
#include "class/clc3b5sw.h"
#include "class/clb069.h"
#include "class/clc670.h"
#include "class/cl9067.h"
#include "class/cl0092.h"
#include "class/clc572.h"
#include "class/clc5b5.h"
#include "class/cl90cdtypes.h"
#include "class/cla06c.h"
#include "class/clc57b.h"
#include "class/cla140.h"
#include "class/cl00de.h"
#include "class/cl9096.h"
#include "class/cl0005_notification.h"
#include "class/cla1c0.h"
#include "class/cla0b5.h"
#include "class/cl917dcrcnotif.h"
#include "class/cl957d.h"
#include "class/clc373.h"
#include "class/clc37a.h"
#include "class/cl90cd.h"
#include "class/clc37dcrcnotif.h"
#include "class/clc5b5sw.h"
#include "class/clc097.h"
#include "class/clc4b0.h"
#include "class/cl5080_notification.h"
#include "class/clc9b0.h"
#include "class/clc638.h"
#include "class/cl5070.h"
#include "class/clc570.h"
#include "class/clc573.h"
#include "class/clc640.h"
#include "class/cl927d.h"
#include "class/clb097.h"
#include "class/clb0b5sw.h"
#include "class/cl00c3.h"
#include "class/clc097tex.h"
#include "class/cl917b.h"
#include "class/clc9fa.h"
#include "class/clc46f.h"
#include "class/clcb97tex.h"
#include "class/clb4b7.h"
#include "class/cl9571.h"
#include "class/cl30f1.h"
#include "class/clc57esw.h"
#include "class/clcbca.h"
#include "class/cl0080_notification.h"
#include "class/cl0070.h"
#include "class/clc639.h"
#include "class/clc67b.h"
#include "class/clc1b7.h"
#include "class/clc6fa.h"
#include "class/clc076.h"
#include "class/cl906fsw.h"
#include "class/clc671.h"
#include "class/cl0005.h"
#include "class/cl917d.h"
#include "class/cl9470.h"
#include "class/clc2b7.h"
#include "class/cl003f.h"
#include "class/cl00fc.h"
#include "class/clc4c0.h"
#include "class/cl987d.h"
#include "class/cl917e.h"
#include "class/cl503c.h"
#include "class/clcb97.h"
#include "class/clc661.h"
#include "class/clb6b0.h"
#include "class/clc77d.h"
#include "class/cl0050.h"
#include "class/clc637.h"
#include "class/cl0076.h"
#include "class/cl402c.h"
#include "class/cla16fsw.h"
#include "class/clb197.h"
#include "class/clc86fsw.h"
#include "class/clc86f.h"
#include "class/cl9074.h"
#include "class/cl900e.h"
#include "class/cl0004.h"
#include "class/cl007d.h"
#include "class/cl5070_notification.h"
#include "class/clc7c0.h"
#include "class/clb0b5.h"
#include "class/cl9170.h"
#include "class/clc561.h"
#include "class/cl90e6.h"
#include "class/clc397.h"
#include "class/cla0b0.h"
#include "class/cl00fd.h"
#include "class/clc37dswspare.h"
#include "class/clc9d1.h"
#include "class/cl2081.h"
#include "class/cl9570.h"
#include "class/cl0002.h"
#include "class/cl0000.h"
#include "class/cl00c2.h"
#include "class/clc5c0.h"
#include "class/clb069sw.h"
#include "class/clc771.h"
#include "class/clc797.h"
#include "class/clc9b7.h"
#include "class/cl83de.h"
#include "class/cl90ce.h"
#include "class/clc58b.h"
#include "class/clc36f.h"
#include "class/cl503b.h"
#include "class/clc37e.h"
#include "class/clc4b7.h"
#include "class/clc461.h"
#include "class/clc673.h"
#include "class/cl0020.h"
#include "class/cld0b7.h"
#include "class/clc7b7.h"
#include "class/clc6b5sw.h"
#include "class/cl208f.h"
#include "class/cl907dswspare.h"
#include "class/clc0c0.h"
#include "class/clc763.h"
#include "class/clc37d.h"
#include "class/clb8d1.h"
#include "class/clc7b5.h"
#endif

#ifdef INCLUDE_CTRL
#include "ctrl/ctrl003e.h"
#include "ctrl/ctrla16f.h"
#include "ctrl/ctrl90e6.h"
#include "ctrl/ctrl0050.h"
#include "ctrl/ctrlxxxx.h"
#include "ctrl/ctrlcb33.h"
#include "ctrl/ctrl0000/ctrl0000nvd.h"
#include "ctrl/ctrl0000/ctrl0000unix.h"
#include "ctrl/ctrl0000/ctrl0000event.h"
#include "ctrl/ctrl0000/ctrl0000gpuacct.h"
#include "ctrl/ctrl0000/ctrl0000client.h"
#include "ctrl/ctrl0000/ctrl0000syncgpuboost.h"
#include "ctrl/ctrl0000/ctrl0000vgpu.h"
#include "ctrl/ctrl0000/ctrl0000gpu.h"
#include "ctrl/ctrl0000/ctrl0000proc.h"
#include "ctrl/ctrl0000/ctrl0000diag.h"
#include "ctrl/ctrl0000/ctrl0000gsync.h"
#include "ctrl/ctrl0000/ctrl0000system.h"
#include "ctrl/ctrl0000/ctrl0000base.h"
#include "ctrl/ctrla06f/ctrla06finternal.h"
#include "ctrl/ctrla06f/ctrla06fevent.h"
#include "ctrl/ctrla06f/ctrla06fbase.h"
#include "ctrl/ctrla06f/ctrla06fgpfifo.h"
#include "ctrl/ctrl00f8.h"
#include "ctrl/ctrlc370/ctrlc370event.h"
#include "ctrl/ctrlc370/ctrlc370rg.h"
#include "ctrl/ctrlc370/ctrlc370chnc.h"
#include "ctrl/ctrlc370/ctrlc370base.h"
#include "ctrl/ctrlc370/ctrlc370verif.h"
#include "ctrl/ctrl0073.h"
#include "ctrl/ctrl83de/ctrl83debase.h"
#include "ctrl/ctrl83de/ctrl83dedebug.h"
#include "ctrl/ctrl9096.h"
#include "ctrl/ctrlc36f.h"
#include "ctrl/ctrl0004.h"
#include "ctrl/ctrl90f1.h"
#include "ctrl/ctrlb0cc.h"
#include "ctrl/ctrl00fd.h"
#include "ctrl/ctrl83de.h"
#include "ctrl/ctrl503c.h"
#include "ctrl/ctrl0090.h"
#include "ctrl/ctrl9074.h"
#include "ctrl/ctrlc763.h"
#include "ctrl/ctrla080.h"
#include "ctrl/ctrl906f.h"
#include "ctrl/ctrlc56f.h"
#include "ctrl/ctrl90cd.h"
#include "ctrl/ctrlb06f.h"
#include "ctrl/ctrla26f.h"
#include "ctrl/ctrl0002.h"
#include "ctrl/ctrlc06f.h"
#include "ctrl/ctrl000f.h"
#include "ctrl/ctrl402c.h"
#include "ctrl/ctrl208f/ctrl208fevent.h"
#include "ctrl/ctrl208f/ctrl208fpmgr.h"
#include "ctrl/ctrl208f/ctrl208fdma.h"
#include "ctrl/ctrl208f/ctrl208fclk.h"
#include "ctrl/ctrl208f/ctrl208fbase.h"
#include "ctrl/ctrl208f/ctrl208fgr.h"
#include "ctrl/ctrl208f/ctrl208fpower.h"
#include "ctrl/ctrl208f/ctrl208ffifo.h"
#include "ctrl/ctrl208f/ctrl208ffb.h"
#include "ctrl/ctrl208f/ctrl208fbus.h"
#include "ctrl/ctrl208f/ctrl208fgpu.h"
#include "ctrl/ctrl208f/ctrl208ffbio.h"
#include "ctrl/ctrl208f/ctrl208fmmu.h"
#include "ctrl/ctrl208f/ctrl208fbif.h"
#include "ctrl/ctrl0080/ctrl0080clk.h"
#include "ctrl/ctrl0080/ctrl0080fb.h"
#include "ctrl/ctrl0080/ctrl0080fifo.h"
#include "ctrl/ctrl0080/ctrl0080host.h"
#include "ctrl/ctrl0080/ctrl0080cipher.h"
#include "ctrl/ctrl0080/ctrl0080perf.h"
#include "ctrl/ctrl0080/ctrl0080gpu.h"
#include "ctrl/ctrl0080/ctrl0080bsp.h"
#include "ctrl/ctrl0080/ctrl0080dma.h"
#include "ctrl/ctrl0080/ctrl0080bif.h"
#include "ctrl/ctrl0080/ctrl0080gr.h"
#include "ctrl/ctrl0080/ctrl0080msenc.h"
#include "ctrl/ctrl0080/ctrl0080internal.h"
#include "ctrl/ctrl0080/ctrl0080nvjpg.h"
#include "ctrl/ctrl0080/ctrl0080base.h"
#include "ctrl/ctrl0080/ctrl0080unix.h"
#include "ctrl/ctrl9067.h"
#include "ctrl/ctrlc372/ctrlc372base.h"
#include "ctrl/ctrlc372/ctrlc372chnc.h"
#include "ctrl/ctrla06f.h"
#include "ctrl/ctrl0020.h"
#include "ctrl/ctrl00fe.h"
#include "ctrl/ctrl208f.h"
#include "ctrl/ctrlb069.h"
#include "ctrl/ctrlc369.h"
#include "ctrl/ctrlc637.h"
#include "ctrl/ctrl9072.h"
#include "ctrl/ctrlc638.h"
#include "ctrl/ctrlb0cc/ctrlb0ccinternal.h"
#include "ctrl/ctrlb0cc/ctrlb0ccpower.h"
#include "ctrl/ctrlb0cc/ctrlb0ccbase.h"
#include "ctrl/ctrlb0cc/ctrlb0ccprofiler.h"
#include "ctrl/ctrla084.h"
#include "ctrl/ctrl00da.h"
#include "ctrl/ctrlc46f.h"
#include "ctrl/ctrl9010.h"
#include "ctrl/ctrlc86f.h"
#include "ctrl/ctrl5080.h"
#include "ctrl/ctrl0073/ctrl0073common.h"
#include "ctrl/ctrl0073/ctrl0073internal.h"
#include "ctrl/ctrl0073/ctrl0073stereo.h"
#include "ctrl/ctrl0073/ctrl0073base.h"
#include "ctrl/ctrl0073/ctrl0073svp.h"
#include "ctrl/ctrl0073/ctrl0073dpu.h"
#include "ctrl/ctrl0073/ctrl0073psr.h"
#include "ctrl/ctrl0073/ctrl0073system.h"
#include "ctrl/ctrl0073/ctrl0073specific.h"
#include "ctrl/ctrl0073/ctrl0073dfp.h"
#include "ctrl/ctrl0073/ctrl0073dp.h"
#include "ctrl/ctrl0073/ctrl0073event.h"
#include "ctrl/ctrl0041.h"
#include "ctrl/ctrl90cc/ctrl90ccpower.h"
#include "ctrl/ctrl90cc/ctrl90ccbase.h"
#include "ctrl/ctrl90cc/ctrl90cchwpm.h"
#include "ctrl/ctrl30f1.h"
#include "ctrl/ctrl2080.h"
#include "ctrl/ctrlc365.h"
#include "ctrl/ctrl2080/ctrl2080pmumon.h"
#include "ctrl/ctrl2080/ctrl2080power.h"
#include "ctrl/ctrl2080/ctrl2080ecc.h"
#include "ctrl/ctrl2080/ctrl2080cipher.h"
#include "ctrl/ctrl2080/ctrl2080gpumon.h"
#include "ctrl/ctrl2080/ctrl2080nvd.h"
#include "ctrl/ctrl2080/ctrl2080base.h"
#include "ctrl/ctrl2080/ctrl2080dmabuf.h"
#include "ctrl/ctrl2080/ctrl2080illum.h"
#include "ctrl/ctrl2080/ctrl2080bios.h"
#include "ctrl/ctrl2080/ctrl2080clkavfs.h"
#include "ctrl/ctrl2080/ctrl2080vgpumgrinternal.h"
#include "ctrl/ctrl2080/ctrl2080i2c.h"
#include "ctrl/ctrl2080/ctrl2080spdm.h"
#include "ctrl/ctrl2080/ctrl2080gr.h"
#include "ctrl/ctrl2080/ctrl2080gpu.h"
#include "ctrl/ctrl2080/ctrl2080fuse.h"
#include "ctrl/ctrl2080/ctrl2080tmr.h"
#include "ctrl/ctrl2080/ctrl2080thermal.h"
#include "ctrl/ctrl2080/ctrl2080clk.h"
#include "ctrl/ctrl2080/ctrl2080mc.h"
#include "ctrl/ctrl2080/ctrl2080vfe.h"
#include "ctrl/ctrl2080/ctrl2080boardobj.h"
#include "ctrl/ctrl2080/ctrl2080boardobjgrpclasses.h"
#include "ctrl/ctrl2080/ctrl2080rc.h"
#include "ctrl/ctrl2080/ctrl2080gsp.h"
#include "ctrl/ctrl2080/ctrl2080fb.h"
#include "ctrl/ctrl2080/ctrl2080lpwr.h"
#include "ctrl/ctrl2080/ctrl2080volt.h"
#include "ctrl/ctrl2080/ctrl2080nvlink.h"
#include "ctrl/ctrl2080/ctrl2080perf.h"
#include "ctrl/ctrl2080/ctrl2080flcn.h"
#include "ctrl/ctrl2080/ctrl2080dma.h"
#include "ctrl/ctrl2080/ctrl2080hshub.h"
#include "ctrl/ctrl2080/ctrl2080acr.h"
#include "ctrl/ctrl2080/ctrl2080fan.h"
#include "ctrl/ctrl2080/ctrl2080internal.h"
#include "ctrl/ctrl2080/ctrl2080fla.h"
#include "ctrl/ctrl2080/ctrl2080perf_cf.h"
#include "ctrl/ctrl2080/ctrl2080pmgr.h"
#include "ctrl/ctrl2080/ctrl2080fifo.h"
#include "ctrl/ctrl2080/ctrl2080perf_cf_pwr_model.h"
#include "ctrl/ctrl2080/ctrl2080spi.h"
#include "ctrl/ctrl2080/ctrl2080unix.h"
#include "ctrl/ctrl2080/ctrl2080grmgr.h"
#include "ctrl/ctrl2080/ctrl2080ce.h"
#include "ctrl/ctrl2080/ctrl2080bus.h"
#include "ctrl/ctrl2080/ctrl2080event.h"
#include "ctrl/ctrl2080/ctrl2080ucodefuzzer.h"
#include "ctrl/ctrl2080/ctrl2080pmu.h"
#include "ctrl/ctrl2080/ctrl2080gpio.h"
#include "ctrl/ctrl5070/ctrl5070event.h"
#include "ctrl/ctrl5070/ctrl5070system.h"
#include "ctrl/ctrl5070/ctrl5070or.h"
#include "ctrl/ctrl5070/ctrl5070base.h"
#include "ctrl/ctrl5070/ctrl5070rg.h"
#include "ctrl/ctrl5070/ctrl5070common.h"
#include "ctrl/ctrl5070/ctrl5070chnc.h"
#include "ctrl/ctrl5070/ctrl5070impoverrides.h"
#include "ctrl/ctrl5070/ctrl5070verif.h"
#include "ctrl/ctrl0080.h"
#include "ctrl/ctrl503c/ctrl503cbase.h"
#include "ctrl/ctrlcbca.h"
#include "ctrl/ctrl506f.h"
#include "ctrl/ctrl90cc.h"
#include "ctrl/ctrl90ec.h"
#include "ctrl/ctrla06c.h"
#include "ctrl/ctrla081.h"
#endif
